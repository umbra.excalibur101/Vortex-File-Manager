@file:Suppress("ktlint")

import org.gradle.api.JavaVersion

object BuildConfig {
    object Type {
        object Release {
            const val Name = "release"
            const val isMinifyEnabled = true
            const val isShrinkResources = true
        }
    }

    object Release {
        const val Name = "release"
    }

    object CMake {
        const val Path = "src/main/cpp/CMakeLists.txt"
    }

    object JDK {
        val VerEnum = JavaVersion.VERSION_21
    }
}
