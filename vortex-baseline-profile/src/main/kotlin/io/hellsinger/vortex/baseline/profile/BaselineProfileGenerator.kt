package io.hellsinger.vortex.baseline.profile

import androidx.benchmark.macro.junit4.BaselineProfileRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

private const val ALLOW_PASCAL_CASE_TEXT = "Allow"
private const val ALLOW_UPPERCASE_TEXT = "ALLOW"
private const val ALLOW_ONLY_WHILE_USING_THE_APP_TEXT = "Allow only while using the app"
private const val WHILE_USING_THE_APP_TEXT = "While using the app"

@RunWith(AndroidJUnit4::class)
@LargeTest
class BaselineProfileGenerator {
    @get:Rule
    val rule = BaselineProfileRule()

    @Test
    fun generate() =
        rule.collect("io.hellsinger.vortex") {
            pressHome()
            startActivityAndWait()
        }
}
