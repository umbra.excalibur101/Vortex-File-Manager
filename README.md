# VortexFileManager (Vortex)

Open and free file manager for Android. In future, with plugins

## Features

    The app currently in active/passive development, so they can be changed in a future

- File operations (e.g. Transactions)
- File observing

## Idea

The main idea - performance.

Yes, performance, cause our team like Linux and it's speed.

Also, we do love open-source and freedom, we hate money :>)

## Special thanks

TODO: Add `Special thanks` -.-

## Screenshots

### List

<p float="left">
  <img alt="Storage List" src="images/storage_list.png" width="250">
  <img alt="Storage List Menu Single item" src="images/storage_list_menu_single.png" width="250">
  <img alt="Storage List Menu Multiple item" src="images/storage_list_menu_multiple.png" width="250">
  <img alt="Storage List Menu Tasks" src="images/storage_list_menu_tasks.png" width="250">
</p>

### Create

<p float="left">
  <img alt="Storage Create Item" src="images/storage_create_item.png" width="250">
</p>

### Transaction

<p float="left">
  <img alt="Storage Transaction Modification" src="images/storage_transaction_modification.png" width="250">
  <img alt="Storage Transaction Modification" src="images/storage_transaction_transition.png" width="250">
</p>
