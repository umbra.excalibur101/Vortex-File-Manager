pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "Vortex-File-Manager"
include(
    ":app",
    ":filesystem",
    ":filesystem:linux",
    ":core:data",
    ":core:domain",
    ":core:settings",
    ":core:di",
    ":core:ui",
    ":core:ui:icon",
    ":core:viewmodel",
    ":core:viewmodel:android",
    ":core:common",
    ":core:navigation",
    ":service",
    ":theme",
    ":theme:vortex",
    ":feature:detail",
    ":feature:editor",
    ":feature:list",
    ":feature:settings",
    ":feature:create",
    ":feature:rename",
    ":feature:schedule",
    ":feature:transaction",
    ":feature:media-viewer",
    ":navigation",
    ":navigation:proccessor",
    ":vortex-foundation",
    ":vortex-baseline-profile",
)

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")
