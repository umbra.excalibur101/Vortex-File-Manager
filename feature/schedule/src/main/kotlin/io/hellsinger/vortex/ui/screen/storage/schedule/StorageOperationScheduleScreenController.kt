package io.hellsinger.vortex.ui.screen.storage.schedule

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout.LayoutParams
import android.widget.LinearLayout
import android.widget.LinearLayout.LayoutParams.MATCH_PARENT
import android.widget.LinearLayout.LayoutParams.WRAP_CONTENT
import io.hellsinger.navigation.screen.ScreenController
import io.hellsinger.vortex.ui.component.item.field.TextFieldItemView
import io.hellsinger.vortex.ui.dsl.field
import io.hellsinger.vortex.ui.dsl.linear
import io.hellsinger.vortex.ui.screen.storage.schedule.StorageOperationScheduleScreenController.Args

class StorageOperationScheduleScreenController(
    private val context: Context,
) : ScreenController<Args>() {
    private var viewModel: StorageOperationScheduleViewModel? = null

    private var root: LinearLayout? = null
    private var time: TextFieldItemView? = null
    private var date: TextFieldItemView? = null
    private var type: TextFieldItemView? = null

    override fun onBackActivatedImpl(): Boolean = false

    override fun onCreateViewImpl(context: Context): View {
        root =
            linear(context) {
                layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
                orientation = LinearLayout.VERTICAL
                gravity = Gravity.BOTTOM
                time =
                    field {
                        hint = "Time"
                        layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                    }
                date =
                    field {
                        hint = "Date"
                    }
                type =
                    field {
                        hint = "Type"
                    }
            }

        return root!!
    }

    override fun onPrepareImpl() {
    }

    override fun onAttachImpl() {
//        val navBarHeight = navigator?.bar?.height ?: return
//        root?.updatePadding(
//            bottom = navBarHeight,
//        )
    }

    override fun onFocusImpl() {
    }

    override fun onHideImpl() {
    }

    override fun onDestroyImpl() {
        root?.removeAllViews()
        root = null
    }

    override fun onSaveImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean = false

    override fun onRestoreImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean = false

    interface Args
}
