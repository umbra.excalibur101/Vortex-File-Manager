package io.hellsinger.vortex.ui.screen.storage.schedule

import io.hellsinger.viewmodel.BitViewModel
import kotlinx.coroutines.Dispatchers

class StorageOperationScheduleViewModel(
    private val dispatchers: Dispatchers,
) : BitViewModel<Unit>("Schedule Scope") {
    override fun getUiState() {}
}
