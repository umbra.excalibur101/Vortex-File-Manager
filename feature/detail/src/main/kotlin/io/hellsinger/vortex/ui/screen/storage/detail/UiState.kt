package io.hellsinger.vortex.ui.screen.storage.detail

import io.hellsinger.vortex.ui.component.adapter.Item

interface UiState {
    val content: List<Item<*>>
}
