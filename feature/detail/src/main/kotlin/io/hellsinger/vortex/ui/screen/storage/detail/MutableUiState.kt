package io.hellsinger.vortex.ui.screen.storage.detail

import io.hellsinger.vortex.ui.component.adapter.Item

data class MutableUiState(
    override val content: MutableList<Item<*>> = mutableListOf(),
) : UiState
