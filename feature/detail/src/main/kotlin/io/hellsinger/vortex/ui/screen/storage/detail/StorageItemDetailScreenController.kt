package io.hellsinger.vortex.ui.screen.storage.detail

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowInsets
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.hellsinger.navigation.screen.ScreenController
import io.hellsinger.viewmodel.BitViewModel
import io.hellsinger.viewmodel.BitViewModel.UiStateUpdateCollector
import io.hellsinger.vortex.data.model.PathItem
import io.hellsinger.vortex.data.model.name
import io.hellsinger.vortex.di.createViewModel
import io.hellsinger.vortex.navigation.globalUiProvider
import io.hellsinger.vortex.ui.component.adapter.ItemAdapter
import io.hellsinger.vortex.ui.component.adapter.listener.ItemListener
import io.hellsinger.vortex.ui.component.item.twoline.TwoLineItemView
import io.hellsinger.vortex.ui.component.navigation.NavigationBarView
import io.hellsinger.vortex.ui.component.observeInsets
import io.hellsinger.vortex.ui.dsl.recycler
import io.hellsinger.vortex.ui.screen.storage.detail.StorageItemDetailViewModel.Companion.UPDATE_ITEMS

class StorageItemDetailScreenController(
    private val context: Context,
) : ScreenController<PathItem>(),
    View.OnApplyWindowInsetsListener,
    ItemListener,
    UiStateUpdateCollector<UiState> {
    private var viewModel: StorageItemDetailViewModel? = null

    private var list: RecyclerView? = null
    private var adapter = ItemAdapter(this)

    private var bar: NavigationBarView? = globalUiProvider(context)?.bar

    override fun onBackActivatedImpl(): Boolean = false

    override fun onCreateViewImpl(context: Context): View {
        list =
            recycler(context) {
                adapter = this@StorageItemDetailScreenController.adapter

                clipToPadding = false
                layoutManager = LinearLayoutManager(context)
                observeInsets(this@StorageItemDetailScreenController)
            }
        return list!!
    }

    override fun onApplyWindowInsets(
        view: View,
        insets: WindowInsets,
    ): WindowInsets {
        val compat = WindowInsetsCompat.toWindowInsetsCompat(insets)
        val statusBar = compat.getInsets(WindowInsetsCompat.Type.statusBars())
        val navigationBar = compat.getInsets(WindowInsetsCompat.Type.navigationBars())
        val bottomPadding = bar?.height ?: navigationBar.bottom

        view.updatePadding(
            top = statusBar.top,
            bottom = bottomPadding,
        )
        return insets
    }

    override suspend fun emit(
        event: Int,
        state: UiState,
    ) {
        if (event == BitViewModel.IDLE) return

        when (event) {
            UPDATE_ITEMS -> {
                bar?.title = args?.name
                adapter.replace(state.content)
            }
        }
    }

    override fun onPrepareImpl() {
        if (viewModel == null) viewModel = context.createViewModel()

        viewModel?.collectUiState(this)

//        navigator?.bar?.replaceItems(emptyList())
        viewModel?.load(item = args ?: return)
    }

    override fun onFocusImpl() {
    }

    override fun onHideImpl() {
        viewModel?.cancelCollectUiState()
    }

    override fun onItemClick(
        view: View,
        position: Int,
    ) {
        when (view) {
            is TwoLineItemView -> {
                val item = args ?: return
            }
        }
    }

    override fun onLongItemClick(
        view: View,
        position: Int,
    ): Boolean = false

    override fun onDestroyImpl() {
        viewModel?.onDestroy()
        viewModel = null
        list?.removeAllViews()
        list = null
    }

    override fun onSaveImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean = false

    override fun onRestoreImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean = false
}
