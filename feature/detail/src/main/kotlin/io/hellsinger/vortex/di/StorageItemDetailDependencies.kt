package io.hellsinger.vortex.di

import android.content.Context
import io.hellsinger.vortex.Dispatchers
import io.hellsinger.vortex.domain.repository.StorageRepository
import io.hellsinger.vortex.navigation.findDependencies
import io.hellsinger.vortex.ui.screen.storage.detail.StorageItemDetailViewModel

interface StorageItemDetailDependencies {
    val dispatchers: Dispatchers
    val repository: StorageRepository
}

internal fun Context.createViewModel(): StorageItemDetailViewModel =
    with(findDependencies<StorageItemDetailDependencies>()) {
        StorageItemDetailViewModel(
            dispatchers = dispatchers,
            repository = repository,
        )
    }
