plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.parcelize)
    alias(libs.plugins.ktlint)
}

ktlint {
    android.set(true)
    outputColorName.set("RED")
}

android {
    namespace = "io.hellsinger.vortex"
    compileSdk = AndroidConfigure.targetSdk

    defaultConfig {
        minSdk = AndroidConfigure.minSdk

        consumerProguardFiles("consumer-rules.pro")
    }

    compileOptions {
        sourceCompatibility = BuildConfig.JDK.VerEnum
        targetCompatibility = BuildConfig.JDK.VerEnum
    }
}

dependencies {
    implementation(libs.androidx.core)
    implementation(libs.androidx.annotations)

    implementation(libs.kotlinx.coroutines.android)

    implementation(projects.filesystem)
    implementation(projects.filesystem.linux)

    implementation(projects.core.ui)
    implementation(projects.core.ui.icon)
    implementation(projects.core.common)
    implementation(projects.core.data)
    implementation(projects.core.viewmodel)
    implementation(projects.core.domain)
    implementation(projects.core.navigation)
    implementation(projects.vortexFoundation)
    implementation(projects.theme)
    implementation(projects.theme.vortex)

    implementation(projects.navigation)
}
