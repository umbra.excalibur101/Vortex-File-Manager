package io.hellsinger.vortex.component.gesture

interface StorageTextEditorScroller {
    fun scrollPage()

    fun scrollLine()
}
