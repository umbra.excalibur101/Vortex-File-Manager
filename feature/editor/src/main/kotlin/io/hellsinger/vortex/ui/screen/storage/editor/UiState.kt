package io.hellsinger.vortex.ui.screen.storage.editor

import io.hellsinger.vortex.data.Line
import io.hellsinger.vortex.data.model.PathItem

interface UiState {
    val item: PathItem?
    val lines: List<Line>
    val encoding: Encoding

    @JvmInline
    value class Encoding(
        val original: String,
    )
}
