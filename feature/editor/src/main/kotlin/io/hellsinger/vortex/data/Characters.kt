package io.hellsinger.vortex.data

object Characters {
    const val DIGITS = "0123456789"

    object Special {
        const val NEW_LINE = '\n'
        const val TAB = '\t'
        const val BACKSPACE = '\b'
        const val RETURN = '\r'

        const val NEW_LINE_BYTE = NEW_LINE.code.toByte()
        const val TAB_BYTE = TAB.code.toByte()
        const val BACKSPACE_BYTE = BACKSPACE.code.toByte()
        const val RETURN_BYTE = RETURN.code.toByte()

        // Line feed
        const val LF = NEW_LINE

        // Carriage return
        const val CR = RETURN

        const val LF_CR = "$LF$CR"
    }
}
