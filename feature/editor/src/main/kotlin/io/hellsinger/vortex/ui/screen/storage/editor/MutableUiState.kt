package io.hellsinger.vortex.ui.screen.storage.editor

import io.hellsinger.vortex.data.Line
import io.hellsinger.vortex.data.model.PathItem
import io.hellsinger.vortex.ui.screen.storage.editor.UiState.Encoding

class MutableUiState(
    override var item: PathItem? = null,
    override val encoding: Encoding = Encoding("UTF-8"),
    override val lines: MutableList<Line> = mutableListOf(),
) : UiState
