package io.hellsinger.vortex.di

import android.content.Context
import io.hellsinger.vortex.Dispatchers
import io.hellsinger.vortex.domain.repository.StorageRepository
import io.hellsinger.vortex.navigation.findDependencies
import io.hellsinger.vortex.ui.screen.storage.editor.StorageFileEditorViewModel

interface StorageFileEditorDependencies {
    val dispatchers: Dispatchers
    val repository: StorageRepository
}

internal fun Context.createViewModel(): StorageFileEditorViewModel =
    with(findDependencies<StorageFileEditorDependencies>()) {
        StorageFileEditorViewModel(
            dispatchers = dispatchers,
            repository = repository,
        )
    }
