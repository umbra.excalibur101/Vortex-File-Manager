package io.hellsinger.vortex.ui.screen.storage.transaction

import io.hellsinger.vortex.ui.component.adapter.SuperItem

interface UiState {
    val items: List<SuperItem>
}
