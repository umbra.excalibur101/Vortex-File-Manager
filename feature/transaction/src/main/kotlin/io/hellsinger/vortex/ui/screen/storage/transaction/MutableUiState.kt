package io.hellsinger.vortex.ui.screen.storage.transaction

import io.hellsinger.vortex.ui.component.adapter.SuperItem

class MutableUiState(
    override val items: MutableList<SuperItem> = mutableListOf(),
) : UiState {
    inline fun updateItems(block: MutableList<SuperItem>.() -> Unit) {
        items.clear()
        items.apply(block)
    }
}
