package io.hellsinger.vortex.di

import android.content.Context
import io.hellsinger.vortex.Dispatchers
import io.hellsinger.vortex.domain.repository.StorageRepository
import io.hellsinger.vortex.navigation.findDependencies
import io.hellsinger.vortex.ui.screen.storage.transaction.StorageTransactionViewModel

interface StorageTransactionDependencies {
    val repository: StorageRepository
    val dispatchers: Dispatchers
}

internal fun Context.createViewModel(): StorageTransactionViewModel =
    with(findDependencies<StorageTransactionDependencies>()) {
        StorageTransactionViewModel(
            dispatchers = dispatchers,
            repository = repository,
        )
    }
