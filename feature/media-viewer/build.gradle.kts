plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.parcelize)
    alias(libs.plugins.ktlint)
}

android {
    namespace = "io.hellsinger.vortex"
    compileSdk = AndroidConfigure.targetSdk

    defaultConfig {
        minSdk = AndroidConfigure.minSdk

        consumerProguardFiles("consumer-rules.pro")
    }

    compileOptions {
        sourceCompatibility = BuildConfig.JDK.VerEnum
        targetCompatibility = BuildConfig.JDK.VerEnum
    }
}

dependencies {
    implementation(libs.androidx.annotations)
    implementation(libs.androidx.viewpager)

    implementation(libs.kotlinx.coroutines.android)

    implementation(projects.filesystem)
    implementation(projects.filesystem.linux)

    implementation(projects.core.ui)
    implementation(projects.core.navigation)
    implementation(projects.core.ui.icon)
    implementation(projects.core.data)
    implementation(projects.core.common)
    implementation(projects.core.viewmodel)
    implementation(projects.core.viewmodel.android)
    implementation(projects.core.domain)
    implementation(projects.vortexFoundation)
    implementation(projects.theme)
    implementation(projects.theme.vortex)

    implementation(projects.navigation)
}
