package io.hellsinger.vortex.ui.screen.storage.media

import android.graphics.Bitmap

interface UiState {
    val image: Bitmap?
}
