package io.hellsinger.vortex.di

import android.content.Context
import io.hellsinger.vortex.Dispatchers
import io.hellsinger.vortex.data.repository.AndroidPermissions
import io.hellsinger.vortex.domain.repository.StorageRepository
import io.hellsinger.vortex.navigation.findDependencies
import io.hellsinger.vortex.ui.screen.storage.media.StorageMediaViewModel

interface StorageMediaDependencies {
    val repository: StorageRepository
    val dispatchers: Dispatchers
    val permissions: AndroidPermissions
}

internal fun Context.createViewModel(): StorageMediaViewModel =
    with(findDependencies<StorageMediaDependencies>()) {
        StorageMediaViewModel(
            dispatchers = dispatchers,
            repository = repository,
        )
    }
