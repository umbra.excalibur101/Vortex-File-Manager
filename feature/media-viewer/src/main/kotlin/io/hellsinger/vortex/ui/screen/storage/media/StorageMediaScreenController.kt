package io.hellsinger.vortex.ui.screen.storage.media

import android.content.Context
import android.os.Bundle
import android.view.View
import io.hellsinger.navigation.screen.HostScreenController
import io.hellsinger.navigation.screen.ScreenController
import io.hellsinger.vortex.data.model.PathItem
import io.hellsinger.vortex.navigation.globalUiProvider
import io.hellsinger.vortex.ui.component.drawable.RoundedCornerDrawable
import io.hellsinger.vortex.ui.screen.storage.media.StorageMediaScreenController.Items

class StorageMediaScreenController(
    private val context: Context,
) : HostScreenController<Items>() {
    private var bar = globalUiProvider(context)?.bar

    override fun getControllerCount(): Int = args?.sources?.size ?: 0

    override fun createScreenController(position: Int): ScreenController<*> {
        val controller = StorageMediaEntryScreenController(context)
        args?.sources?.get(position)?.let { item -> controller.setArgs(item) }
        return controller
    }

    override fun onCreateViewImpl(context: Context): View {
        val root = super.onCreateViewImpl(context)
        root.background = RoundedCornerDrawable(0xFF000000)
        return root
    }

    override fun onPrepareImpl() {
        bar?.replaceItems(emptyList())
    }

    override fun onFocusImpl() {
    }

    override fun onHideImpl() {
    }

    override fun onDestroyImpl() {
        adapter.destroy()
    }

    override fun onSaveImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean = false

    override fun onRestoreImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean = false

    data class Items(
        val sources: List<PathItem>,
        val selected: Int,
    )
}
