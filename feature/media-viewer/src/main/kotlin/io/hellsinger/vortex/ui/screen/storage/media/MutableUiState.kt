package io.hellsinger.vortex.ui.screen.storage.media

import android.graphics.Bitmap

internal class MutableUiState(
    override var image: Bitmap? = null,
) : UiState
