package io.hellsinger.vortex.ui.screen.settings

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.WindowInsets
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.hellsinger.navigation.screen.ScreenController
import io.hellsinger.theme.vortex.VortexSettings
import io.hellsinger.vortex.navigation.globalUiProvider
import io.hellsinger.vortex.ui.component.adapter.ItemAdapter
import io.hellsinger.vortex.ui.component.adapter.Items
import io.hellsinger.vortex.ui.component.adapter.UnsupportedViewTypeException
import io.hellsinger.vortex.ui.component.adapter.listener.ItemListener
import io.hellsinger.vortex.ui.component.item.title.TitleItemView
import io.hellsinger.vortex.ui.component.item.twoline.TwoLineItemView
import io.hellsinger.vortex.ui.component.observeInsets
import io.hellsinger.vortex.ui.dsl.params
import io.hellsinger.vortex.ui.dsl.recycler

class SettingsEntryScreenController(
    context: Context,
    renderer: SettingsOptionsRenderer,
) : ScreenController<Int>(),
    View.OnApplyWindowInsetsListener,
    ItemListener {
    private var list: RecyclerView? = null
    private val adapter =
        ItemAdapter(
            listener = this,
            renderer = renderer,
            viewProvider = { parent, viewType ->
                when (viewType) {
                    Items.TWO_LINE ->
                        TwoLineItemView(parent.context).apply {
                            setTitleColor(VortexSettings.colors { key_storage_transaction_two_line_title_color })
                            setDescriptionColor(VortexSettings.colors { key_storage_transaction_two_line_description_color })
                            setRippleColor(VortexSettings.colors { key_storage_transaction_two_line_ripple_color })
                            setBackgroundColor(VortexSettings.colors { key_storage_transaction_two_line_surface_color })
                        }

                    Items.TITLE ->
                        TitleItemView(parent.context).apply {
                            setTitleColor(VortexSettings.colors { key_storage_transaction_title_color })
                        }

                    else -> throw UnsupportedViewTypeException(viewType)
                }
            },
        )
    private val bar = globalUiProvider(context)?.bar

    override fun onItemClick(
        view: View,
        position: Int,
    ) {
    }

    override fun onLongItemClick(
        view: View,
        position: Int,
    ): Boolean = false

    override fun onBackActivatedImpl(): Boolean = false

    override fun onCreateViewImpl(context: Context): View {
        list =
            recycler(context) {
                params(MATCH_PARENT, MATCH_PARENT)
                adapter = this@SettingsEntryScreenController.adapter
                layoutManager = LinearLayoutManager(context)
                clipToPadding = false
                observeInsets(this@SettingsEntryScreenController)
                addOnScrollListener(
                    object : RecyclerView.OnScrollListener() {
                        override fun onScrolled(
                            recyclerView: RecyclerView,
                            dx: Int,
                            dy: Int,
                        ) {
                            if (dy > 0) {
                                bar?.hide()
                            } else {
                                bar?.show()
                            }
                        }

                        override fun onScrollStateChanged(
                            recyclerView: RecyclerView,
                            newState: Int,
                        ) {
                        }
                    },
                )
            }

        return list!!
    }

    override fun onPrepareImpl() {
    }

    override fun onApplyWindowInsets(
        v: View,
        insets: WindowInsets,
    ): WindowInsets {
        val compat = WindowInsetsCompat.toWindowInsetsCompat(insets)
        val statusBar = compat.getInsets(WindowInsetsCompat.Type.statusBars())
        val navigationBar = compat.getInsets(WindowInsetsCompat.Type.navigationBars())
        val appBarHeight = bar?.height ?: 0

        v.updatePadding(
            top = statusBar.top,
            bottom = navigationBar.bottom + appBarHeight,
        )
        return insets
    }

    override fun onFocusImpl() {
    }

    override fun onHideImpl() {
    }

    override fun onDestroyImpl() {
        list?.removeAllViews()
        list = null
    }

    override fun onSaveImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean = false

    override fun onRestoreImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean = false
}
