package io.hellsinger.vortex.ui.screen.settings.text

import io.hellsinger.theme.vortex.VortexSettings
import io.hellsinger.theme.vortex.VortexTextsKeys
import io.hellsinger.vortex.ui.component.adapter.Item
import io.hellsinger.vortex.ui.component.adapter.SuperItem
import io.hellsinger.vortex.ui.component.adapter.title
import io.hellsinger.vortex.ui.component.adapter.twoLine
import io.hellsinger.vortex.ui.screen.settings.SettingsOptionsRenderer

class TextSettingsOptionsRenderer : SettingsOptionsRenderer {
    override fun render(): List<Item<*>> =
        buildList {
            title("Navigation drawer")
            VortexSettings.settingsTextOption("navigation_drawer_title") { key_navigation_drawer_title }
            VortexSettings.settingsTextOption("navigation_drawer_item_file_manager") { key_navigation_drawer_item_file_manager }
            VortexSettings.settingsTextOption("navigation_drawer_item_settings") { key_navigation_drawer_item_settings }

            title("Storage item name")
            VortexSettings.settingsTextOption("storage_path_item_name_external_storage") { key_storage_path_item_name_external_storage }

            title("Storage item mimetype")
            VortexSettings.settingsTextOption("storage_path_item_mime_type_application") { key_storage_path_item_mime_type_application }
            VortexSettings.settingsTextOption("storage_path_item_mime_type_image") { key_storage_path_item_mime_type_image }
            VortexSettings.settingsTextOption("storage_path_item_mime_type_video") { key_storage_path_item_mime_type_video }
            VortexSettings.settingsTextOption("storage_path_item_mime_type_audio") { key_storage_path_item_mime_type_audio }
            VortexSettings.settingsTextOption("storage_path_item_mime_type_text") { key_storage_path_item_mime_type_text }

            title("Storage item size")
            VortexSettings.settingsTextOption("storage_path_item_size_byte") { key_storage_path_item_size_byte }
            VortexSettings.settingsTextOption("storage_path_item_size_kilo_byte") { key_storage_path_item_size_kilo_byte }
            VortexSettings.settingsTextOption("storage_path_item_size_mega_byte") { key_storage_path_item_size_mega_byte }
            VortexSettings.settingsTextOption("storage_path_item_size_giga_byte") { key_storage_path_item_size_giga_byte }
            VortexSettings.settingsTextOption("storage_path_item_size_tera_byte") { key_storage_path_item_size_tera_byte }
            VortexSettings.settingsTextOption("storage_path_item_size_peta_byte") { key_storage_path_item_size_peta_byte }
            VortexSettings.settingsTextOption("storage_path_item_size_ebi_byte") { key_storage_path_item_size_ebi_byte }
            VortexSettings.settingsTextOption("storage_path_item_size_zetta_byte") { key_storage_path_item_size_zetta_byte }
            VortexSettings.settingsTextOption("storage_path_item_size_yotta_byte") { key_storage_path_item_size_yotta_byte }

            title("Storage list info")
            VortexSettings.settingsTextOption("storage_list_info_message_empty") { key_storage_list_info_message_empty }
            VortexSettings.settingsTextOption("storage_list_info_message_restricted") { key_storage_list_info_message_restricted }
            VortexSettings.settingsTextOption("storage_list_info_message_access") { key_storage_list_info_message_access }
            VortexSettings.settingsTextOption("storage_list_info_message_full_access") { key_storage_list_info_message_full_access }

            title("Storage list item description")
            VortexSettings.settingsTextOption("storage_list_item_description_separator") { key_storage_list_item_description_separator }
            VortexSettings.settingsTextOption("storage_list_item_description_placeholder") { key_storage_list_item_description_placeholder }
            VortexSettings.settingsTextOption("storage_list_item_description_restricted") { key_storage_list_item_description_restricted }

            title("Storage list item directory count")
            VortexSettings.settingsTextOption(
                "storage_list_item_directory_multiple_count",
            ) { key_storage_list_item_directory_multiple_count }
            VortexSettings.settingsTextOption("storage_list_item_directory_single_count") { key_storage_list_item_directory_single_count }
            VortexSettings.settingsTextOption("storage_list_item_directory_zero_count") { key_storage_list_item_directory_zero_count }

            title("Storage list action")
            VortexSettings.settingsTextOption(
                "storage_list_navigation_action_provide_access",
            ) { key_storage_list_navigation_action_provide_access }
            VortexSettings.settingsTextOption("storage_list_navigation_action_provide_full_access") {
                key_storage_list_navigation_action_provide_full_access
            }
            VortexSettings.settingsTextOption("storage_list_navigation_action_search") { key_storage_list_navigation_action_search }
            VortexSettings.settingsTextOption("storage_list_navigation_action_more") { key_storage_list_navigation_action_more }
            VortexSettings.settingsTextOption("storage_list_navigation_action_tasks") { key_storage_list_navigation_action_tasks }

            title("Storage list menu")
            VortexSettings.settingsTextOption("storage_list_menu_primary_title") { key_storage_list_menu_primary_title }
            VortexSettings.settingsTextOption("storage_list_menu_secondary_title") { key_storage_list_menu_secondary_title }
            VortexSettings.settingsTextOption("storage_list_menu_operation_open") { key_storage_list_menu_operation_open }
            VortexSettings.settingsTextOption("storage_list_menu_operation_add_new") { key_storage_list_menu_operation_add_new }
            VortexSettings.settingsTextOption("storage_list_menu_operation_delete") { key_storage_list_menu_operation_delete }
            VortexSettings.settingsTextOption("storage_list_menu_operation_rename") { key_storage_list_menu_operation_rename }
            VortexSettings.settingsTextOption("storage_list_menu_operation_copy") { key_storage_list_menu_operation_copy }
            VortexSettings.settingsTextOption("storage_list_menu_operation_move") { key_storage_list_menu_operation_move }
            VortexSettings.settingsTextOption("storage_list_menu_operation_detail") { key_storage_list_menu_operation_detail }
            VortexSettings.settingsTextOption("storage_list_menu_operation_swap") { key_storage_list_menu_operation_swap }
            VortexSettings.settingsTextOption("storage_list_menu_operation_archive") { key_storage_list_menu_operation_archive }
            VortexSettings.settingsTextOption("storage_list_menu_operation_copy_path") { key_storage_list_menu_operation_copy_path }

            title("Storage service")
            VortexSettings.settingsTextOption("storage_service_item_deleted_message") { key_storage_service_item_deleted_message }
            VortexSettings.settingsTextOption("key_storage_service_item_created_message") { key_storage_service_item_created_message }
            VortexSettings.settingsTextOption(
                "storage_service_vortex_channel_description",
            ) { key_storage_service_vortex_channel_description }
            VortexSettings.settingsTextOption("storage_service_operation_channel_description") {
                key_storage_service_operation_channel_description
            }
        }
}

context(MutableList<SuperItem>)
inline fun VortexSettings.settingsTextOption(
    title: String,
    picker: VortexTextsKeys.() -> Int,
) = twoLine(
    title,
    texts(picker),
)
