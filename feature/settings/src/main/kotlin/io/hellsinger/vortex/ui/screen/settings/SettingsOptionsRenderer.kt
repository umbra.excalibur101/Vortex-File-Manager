package io.hellsinger.vortex.ui.screen.settings

import io.hellsinger.vortex.ui.component.adapter.ListRenderer

// Interface marker
interface SettingsOptionsRenderer : ListRenderer
