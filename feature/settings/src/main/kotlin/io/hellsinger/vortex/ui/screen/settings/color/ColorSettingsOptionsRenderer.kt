package io.hellsinger.vortex.ui.screen.settings.color

import io.hellsinger.theme.vortex.VortexColorsKeys
import io.hellsinger.theme.vortex.VortexSettings
import io.hellsinger.vortex.ui.component.adapter.SuperItem
import io.hellsinger.vortex.ui.component.adapter.title
import io.hellsinger.vortex.ui.component.adapter.twoLine
import io.hellsinger.vortex.ui.screen.settings.SettingsOptionsRenderer
import kotlin.text.HexFormat.Companion.UpperCase

class ColorSettingsOptionsRenderer : SettingsOptionsRenderer {
    override fun render(): List<SuperItem> =
        buildList {
            title("Navigation bar")
            VortexSettings.settingsColorOption("navigation_bar_surface_color") { key_navigation_bar_surface_color }
            VortexSettings.settingsColorOption("navigation_bar_title_color") { key_navigation_bar_title_color }
            VortexSettings.settingsColorOption("navigation_bar_subtitle_color") { key_navigation_bar_subtitle_color }
            VortexSettings.settingsColorOption("navigation_bar_icon_color") { key_navigation_bar_icon_color }
            VortexSettings.settingsColorOption("navigation_bar_icon_ripple_color") { key_navigation_bar_icon_ripple_color }

            title("Navigation drawer")
            VortexSettings.settingsColorOption("navigation_drawer_background_color") { key_navigation_drawer_background_color }
            VortexSettings.settingsColorOption("navigation_drawer_title_color") { key_navigation_drawer_title_color }
            VortexSettings.settingsColorOption("navigation_drawer_item_surface_color") { key_navigation_drawer_item_surface_color }
            VortexSettings.settingsColorOption("navigation_drawer_item_icon_color") { key_navigation_drawer_item_icon_color }
            VortexSettings.settingsColorOption("navigation_drawer_item_title_color") { key_navigation_drawer_item_title_color }
            VortexSettings.settingsColorOption("navigation_drawer_item_ripple_color") { key_navigation_drawer_item_ripple_color }

            title("Storage list")
            VortexSettings.settingsColorOption("storage_list_background_color") { key_storage_list_background_color }

            title("Storage list item")
            VortexSettings.settingsColorOption("storage_list_item_surface_color") { key_storage_list_item_surface_color }
            VortexSettings.settingsColorOption("storage_list_item_surface_ripple_color") { key_storage_list_item_surface_ripple_color }
            VortexSettings.settingsColorOption("storage_list_item_title_color") { key_storage_list_item_title_color }
            VortexSettings.settingsColorOption("storage_list_item_subtitle_color") { key_storage_list_item_subtitle_color }
            VortexSettings.settingsColorOption("storage_list_item_icon_color") { key_storage_list_item_icon_color }
            VortexSettings.settingsColorOption("storage_list_item_icon_surface_color") { key_storage_list_item_icon_surface_color }
            VortexSettings.settingsColorOption("storage_list_item_selected_color") { key_storage_list_item_selected_color }

            title("Storage trail item")
            VortexSettings.settingsColorOption("storage_list_trail_surface_color") { key_storage_list_trail_surface_color }
            VortexSettings.settingsColorOption("storage_list_trail_item_text_color") { key_storage_list_trail_item_text_color }
            VortexSettings.settingsColorOption("storage_list_trail_item_arrow_color") { key_storage_list_trail_item_arrow_color }
            VortexSettings.settingsColorOption("storage_list_trail_item_ripple_color") { key_storage_list_trail_item_ripple_color }
            VortexSettings.settingsColorOption(
                "storage_list_trail_item_text_selected_color",
            ) { key_storage_list_trail_item_text_selected_color }
            VortexSettings.settingsColorOption(
                "storage_list_trail_item_arrow_selected_color",
            ) { key_storage_list_trail_item_arrow_selected_color }
            VortexSettings.settingsColorOption(
                "storage_list_trail_item_ripple_selected_color",
            ) { key_storage_list_trail_item_ripple_selected_color }

            title("Storage list loading")
            VortexSettings.settingsColorOption("storage_list_loading_title_color") { key_storage_list_loading_title_color }
            VortexSettings.settingsColorOption("storage_list_loading_progress_color") { key_storage_list_loading_progress_color }

            title("Storage list info")
            VortexSettings.settingsColorOption("storage_list_info_message_color") { key_storage_list_info_message_color }
            VortexSettings.settingsColorOption("storage_list_info_icon_color") { key_storage_list_info_icon_color }

            title("Storage list menu")
            VortexSettings.settingsColorOption("storage_list_menu_background_color") { key_storage_list_menu_background_color }
            VortexSettings.settingsColorOption("storage_list_menu_title_color") { key_storage_list_menu_title_color }
            VortexSettings.settingsColorOption("storage_list_menu_drawer_surface_color") { key_storage_list_menu_drawer_surface_color }
            VortexSettings.settingsColorOption("storage_list_menu_drawer_icon_color") { key_storage_list_menu_drawer_icon_color }
            VortexSettings.settingsColorOption("storage_list_menu_drawer_title_color") { key_storage_list_menu_drawer_title_color }
            VortexSettings.settingsColorOption("storage_list_menu_drawer_ripple_color") { key_storage_list_menu_drawer_ripple_color }
            VortexSettings.settingsColorOption("storage_list_menu_divider_color") { key_storage_list_menu_divider_color }
            VortexSettings.settingsColorOption("storage_list_menu_two_line_surface_color") { key_storage_list_menu_two_line_surface_color }
            VortexSettings.settingsColorOption("storage_list_menu_two_line_title_color") { key_storage_list_menu_two_line_title_color }
            VortexSettings.settingsColorOption(
                "storage_list_menu_two_line_description_color",
            ) { key_storage_list_menu_two_line_description_color }
            VortexSettings.settingsColorOption("storage_list_menu_two_line_ripple_color") { key_storage_list_menu_two_line_ripple_color }

            title("Storage list menu task")
            VortexSettings.settingsColorOption("storage_list_menu_task_background_color") { key_storage_list_menu_task_background_color }
            VortexSettings.settingsColorOption("storage_list_menu_task_title_color") { key_storage_list_menu_task_title_color }
            VortexSettings.settingsColorOption(
                "storage_list_menu_task_item_surface_color",
            ) { key_storage_list_menu_task_item_surface_color }
            VortexSettings.settingsColorOption("storage_list_menu_task_item_title_color") { key_storage_list_menu_task_item_title_color }
            VortexSettings.settingsColorOption(
                "storage_list_menu_task_item_description_color",
            ) { key_storage_list_menu_task_item_description_color }
            VortexSettings.settingsColorOption("storage_list_menu_task_item_ripple_color") { key_storage_list_menu_task_item_ripple_color }

            title("Storage create")
            VortexSettings.settingsColorOption("storage_create_item_background_color") { key_storage_create_item_background_color }
            VortexSettings.settingsColorOption("storage_create_item_field_cursor_color") { key_storage_create_item_field_cursor_color }
            VortexSettings.settingsColorOption("storage_create_item_field_line_color") { key_storage_create_item_field_line_color }
            VortexSettings.settingsColorOption("storage_create_item_field_hint_color") { key_storage_create_item_field_hint_color }
            VortexSettings.settingsColorOption("storage_create_item_field_text_color") { key_storage_create_item_field_text_color }

            title("Storage transaction")
            VortexSettings.settingsColorOption("storage_transaction_background_color") { key_storage_transaction_background_color }
            VortexSettings.settingsColorOption("storage_transaction_title_color") { key_storage_transaction_title_color }
            VortexSettings.settingsColorOption(
                "storage_transaction_two_line_surface_color",
            ) { key_storage_transaction_two_line_surface_color }
            VortexSettings.settingsColorOption("storage_transaction_two_line_title_color") { key_storage_transaction_two_line_title_color }
            VortexSettings.settingsColorOption(
                "storage_transaction_two_line_description_color",
            ) { key_storage_transaction_two_line_description_color }
            VortexSettings.settingsColorOption(
                "storage_transaction_two_line_ripple_color",
            ) { key_storage_transaction_two_line_ripple_color }
        }
}

context(MutableList<SuperItem>)
@OptIn(ExperimentalStdlibApi::class)
inline fun VortexSettings.settingsColorOption(
    title: String,
    picker: VortexColorsKeys.() -> Int,
) = twoLine(
    title,
    "#${colors(picker).toHexString(UpperCase)}",
)
