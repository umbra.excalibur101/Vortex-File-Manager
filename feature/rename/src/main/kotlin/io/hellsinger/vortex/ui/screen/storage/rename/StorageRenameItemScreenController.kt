package io.hellsinger.vortex.ui.screen.storage.rename

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.LinearLayout.LayoutParams.MATCH_PARENT
import android.widget.LinearLayout.LayoutParams.WRAP_CONTENT
import io.hellsinger.filesystem.attribute.DefaultPathAttribute
import io.hellsinger.navigation.screen.ScreenController
import io.hellsinger.vortex.R
import io.hellsinger.vortex.data.model.PathItem
import io.hellsinger.vortex.data.model.name
import io.hellsinger.vortex.di.createViewModel
import io.hellsinger.vortex.navigation.globalUiProvider
import io.hellsinger.vortex.ui.component.item.field.TextFieldItemView
import io.hellsinger.vortex.ui.component.item.menu.MenuAction
import io.hellsinger.vortex.ui.component.item.menu.MenuActionListener
import io.hellsinger.vortex.ui.component.navigation.NavigationBarView
import io.hellsinger.vortex.ui.dsl.field
import io.hellsinger.vortex.ui.dsl.linear
import io.hellsinger.vortex.ui.dsl.linearParams
import io.hellsinger.vortex.ui.icon.Icons
import io.hellsinger.vortex.ui.screen.storage.transaction.StorageTransactionScreenController
import io.hellsinger.vortex.ui.screen.storage.transaction.StorageTransactionScreenController.Args.Companion.LOAD_TYPE_RENAME
import io.hellsinger.vortex.ui.screen.storage.transaction.StorageTransactionScreenController.Args.Transition

class StorageRenameItemScreenController(
    private val context: Context,
) : ScreenController<PathItem>(),
    MenuActionListener {
    private var root: LinearLayout? = null
    private var field: TextFieldItemView? = null
    private var bar: NavigationBarView? = globalUiProvider(context)?.bar

    private var viewModel: StorageRenameItemViewModel? = null

    override fun onBackActivatedImpl(): Boolean = false

    override fun onCreateViewImpl(context: Context): View {
        root =
            linear(context) {
                field =
                    field {
                        linearParams(MATCH_PARENT, WRAP_CONTENT)
                    }
            }
        return root!!
    }

    override fun onPrepareImpl() {
        if (viewModel == null) viewModel = context.createViewModel()

        val item = args ?: return
        viewModel?.load(item)
        field?.text = item.name
        bar?.setTitleWithAnimation("Rename ${item.name}")
        bar?.replaceItems(
            listOf(
                MenuAction(
                    R.id.file_manager_rename_proceed,
                    "Proceed",
                    Icons.Rounded.ArrowRight,
                ),
            ),
        )
        bar?.registerListener(this)
    }

    override fun onFocusImpl() {
    }

    override fun onHideImpl() {
        bar?.unregisterListener(this)
    }

    override fun onDestroyImpl() {
        bar?.unregisterListener(this)
        field = null
        bar = null
    }

    override fun onMenuActionCall(id: Int): Boolean {
        when (id) {
            R.id.file_manager_rename_proceed -> {
                val item = args ?: return true
                val name = field?.text ?: return true
                val destPath = item.parent?.path?.resolve(name.encodeToByteArray()) ?: return true
                val dest =
                    PathItem(
                        destPath,
                        DefaultPathAttribute(
                            type = item.type,
                            size = item.attrs.size,
                            permission = item.permission,
                        ),
                    )
                router?.navigate(
                    StorageTransactionScreenController(context),
                    Transition(listOf(item), dest, LOAD_TYPE_RENAME),
                )
                return true
            }
        }
        return false
    }

    override fun onSaveImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean = false

    override fun onRestoreImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean = false
}
