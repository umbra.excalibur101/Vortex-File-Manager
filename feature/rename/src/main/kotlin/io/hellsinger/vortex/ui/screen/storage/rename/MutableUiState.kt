package io.hellsinger.vortex.ui.screen.storage.rename

data class MutableUiState(
    override var name: String = "",
) : UiState
