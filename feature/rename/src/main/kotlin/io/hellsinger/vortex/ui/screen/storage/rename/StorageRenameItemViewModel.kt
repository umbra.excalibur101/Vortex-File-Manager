package io.hellsinger.vortex.ui.screen.storage.rename

import io.hellsinger.viewmodel.VortexViewModel
import io.hellsinger.vortex.Dispatchers
import io.hellsinger.vortex.data.model.PathItem

class StorageRenameItemViewModel(
    private val dispatchers: Dispatchers,
) : VortexViewModel<UiState, MutableUiState>(scopeName = "Storage Rename Item Scope") {
    override val state: MutableUiState = MutableUiState()

    fun load(item: PathItem) {}
}
