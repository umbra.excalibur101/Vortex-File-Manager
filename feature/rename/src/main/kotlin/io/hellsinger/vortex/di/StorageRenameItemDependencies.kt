package io.hellsinger.vortex.di

import android.content.Context
import io.hellsinger.vortex.Dispatchers
import io.hellsinger.vortex.domain.repository.StorageRepository
import io.hellsinger.vortex.navigation.findDependencies
import io.hellsinger.vortex.ui.screen.storage.rename.StorageRenameItemViewModel

interface StorageRenameItemDependencies {
    val dispatchers: Dispatchers
    val repository: StorageRepository
}

internal fun Context.createViewModel(): StorageRenameItemViewModel =
    with(findDependencies<StorageRenameItemDependencies>()) {
        StorageRenameItemViewModel(
            dispatchers = dispatchers,
        )
    }
