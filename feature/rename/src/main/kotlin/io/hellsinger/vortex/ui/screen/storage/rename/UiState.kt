package io.hellsinger.vortex.ui.screen.storage.rename

interface UiState {
    val name: String
}
