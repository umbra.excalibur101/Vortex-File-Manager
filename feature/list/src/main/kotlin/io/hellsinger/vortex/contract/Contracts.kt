package io.hellsinger.vortex.contract

import android.content.Context
import android.content.Intent
import android.net.Uri.fromParts
import android.os.Build
import android.provider.Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION
import androidx.annotation.RequiresApi

object Contracts {
    object Storage {
        const val REQUEST_STORAGE_ACCESS = 0

        @RequiresApi(Build.VERSION_CODES.R)
        const val REQUEST_FULL_STORAGE_ACCESS = 1

        @RequiresApi(Build.VERSION_CODES.R)
        @Suppress("ktlint:standard:function-naming")
        fun StorageFullAccessIntent(context: Context): Intent =
            Intent(ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
                .setData(fromParts("package", context.packageName, null))
                .addCategory("android.intent.category.DEFAULT")
    }
}
