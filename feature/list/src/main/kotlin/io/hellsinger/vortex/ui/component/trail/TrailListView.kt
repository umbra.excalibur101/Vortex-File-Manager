package io.hellsinger.vortex.ui.component.trail

import android.content.Context
import android.graphics.Paint
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.hellsinger.vortex.bit.Bits.Companion.addFlag
import io.hellsinger.vortex.ui.component.ViewMeasure
import io.hellsinger.vortex.ui.component.adapter.decoration.MarginItemDecoration
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.drawable.RoundedCornerDrawable

class TrailListView(
    context: Context,
) : RecyclerView(context) {
    private val surface =
        RoundedCornerDrawable(
            Paint(Paint.ANTI_ALIAS_FLAG addFlag Paint.DITHER_FLAG),
        ).apply {
            setElevation(4F.dp)
        }

    init {
        background = surface
        layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
        addItemDecoration(MarginItemDecoration(horizontal = 4.dp))
    }

    override fun setBackgroundColor(color: Int) {
        surface.setColor(color)
    }

    override fun setElevation(elevation: Float) {
        surface.setElevation(elevation)
    }

    override fun onMeasure(
        widthSpec: Int,
        heightSpec: Int,
    ) = ViewMeasure(
        widthSpec = widthSpec,
        heightSpec = heightSpec,
        desireWidth = ViewGroup.LayoutParams.MATCH_PARENT,
        desireHeight = 40,
    ) { width, height ->
        setMeasuredDimension(
            width - paddingLeft + paddingRight,
            height + paddingTop - paddingBottom,
        )
    }
}
