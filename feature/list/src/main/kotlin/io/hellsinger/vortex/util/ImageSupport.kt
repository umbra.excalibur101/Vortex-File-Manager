package io.hellsinger.vortex.util

import io.hellsinger.vortex.data.model.PathItem

fun PathItem.hasImageSupportableFormat(): Boolean =
    when (mime.extension) {
        "bmp" -> true
        "gif" -> true
        "png" -> true
        "jpeg" -> true
        "jpg" -> true
        "webp" -> true
        else -> false
    }
