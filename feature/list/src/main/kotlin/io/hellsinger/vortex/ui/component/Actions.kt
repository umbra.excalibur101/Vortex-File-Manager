package io.hellsinger.vortex.ui.component

import android.os.Build
import androidx.annotation.RequiresApi
import io.hellsinger.theme.vortex.VortexSettings
import io.hellsinger.vortex.ui.component.item.menu.MenuAction
import io.hellsinger.vortex.ui.icon.Icons

val ProvideFullStorageAccessAction: MenuAction
    @RequiresApi(Build.VERSION_CODES.R)
    get() =
        MenuAction(
            id = io.hellsinger.vortex.R.id.file_manager_list_provide_full_storage_access_action,
            title = VortexSettings.texts { key_storage_list_navigation_action_provide_full_access },
            icon = Icons.Rounded.Check,
        )

val ProvideStorageAccessAction: MenuAction
    get() =
        MenuAction(
            id = io.hellsinger.vortex.R.id.file_manager_list_provide_storage_access_action,
            title = VortexSettings.texts { key_storage_list_navigation_action_provide_access },
            icon = Icons.Rounded.Check,
        )

val SearchAction: MenuAction
    get() =
        MenuAction(
            id = io.hellsinger.vortex.R.id.file_manager_list_search_action,
            title = VortexSettings.texts { key_storage_list_navigation_action_search },
            icon = Icons.Rounded.Search,
        )

val MoreAction: MenuAction
    get() =
        MenuAction(
            id = io.hellsinger.vortex.R.id.file_manager_list_more_action,
            title = VortexSettings.texts { key_storage_list_navigation_action_more },
            icon = Icons.Rounded.More,
        )

val TasksAction: MenuAction
    get() =
        MenuAction(
            id = io.hellsinger.vortex.R.id.file_manager_list_tasks_action,
            title = VortexSettings.texts { key_storage_list_navigation_action_tasks },
            icon = Icons.Rounded.Tasks,
        )
