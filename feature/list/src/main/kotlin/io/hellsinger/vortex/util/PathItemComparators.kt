package io.hellsinger.vortex.util

import io.hellsinger.vortex.data.model.PathItem
import io.hellsinger.vortex.data.model.name

object PathItemComparators {
    inline val Name: Comparator<PathItem>
        get() =
            compareByDescending(PathItem::isDirectory).thenBy { item ->
                item.name
            }

    inline val Path: Comparator<PathItem>
        get() = compareByDescending(PathItem::isDirectory).thenBy(PathItem::path)

    inline val LastModifiedTime: Comparator<PathItem>
        get() =
            compareByDescending(
                PathItem::isDirectory,
            ).thenBy(PathItem::lastModifiedTime)

    inline val LastAccessTime: Comparator<PathItem>
        get() =
            compareByDescending(
                PathItem::isDirectory,
            ).thenBy(PathItem::lastAccessTime)

    inline val CreationTime: Comparator<PathItem>
        get() =
            compareByDescending(
                PathItem::isDirectory,
            ).thenBy(PathItem::creationTime)
}
