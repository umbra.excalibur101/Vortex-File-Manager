package io.hellsinger.vortex.ui.component.trail

import androidx.recyclerview.widget.DiffUtil
import io.hellsinger.vortex.data.model.PathItem

class TrailDiffer : DiffUtil.ItemCallback<PathItem>() {
    override fun areItemsTheSame(
        oldItem: PathItem,
        newItem: PathItem,
    ): Boolean = oldItem.hashCode() == newItem.hashCode()

    override fun areContentsTheSame(
        oldItem: PathItem,
        newItem: PathItem,
    ): Boolean = oldItem == newItem
}
