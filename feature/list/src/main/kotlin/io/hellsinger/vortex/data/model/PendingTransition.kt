package io.hellsinger.vortex.data.model

import io.hellsinger.vortex.data.model.PendingTransition.Companion.TYPE_COPY
import io.hellsinger.vortex.data.model.PendingTransition.Companion.TYPE_MOVE
import io.hellsinger.vortex.ui.component.adapter.Item
import io.hellsinger.vortex.ui.component.adapter.Items
import io.hellsinger.vortex.ui.component.item.twoline.TwoLineItem

data class PendingTransition(
    override val value: List<PathItem>,
    val type: Int,
) : Item<List<PathItem>> {
    override val id: Long
        get() = hashCode().toLong()

    override val viewType: Int
        get() = Items.PENDING_TRANSITION

    companion object {
        const val TYPE_COPY = 0
        const val TYPE_MOVE = 1
    }
}

fun PendingTransition.toTwoLine(dest: PathItem? = null): TwoLineItem =
    when (type) {
        TYPE_MOVE -> {
            if (value.size == 1) {
                TwoLineItem(
                    title = "Move ${value.single().name} to ${dest?.name}",
                    description = "Tap to proceed transition",
                )
            } else {
                val names =
                    value
                        .take(5)
                        .joinToString { source -> source.name }

                TwoLineItem(
                    title = "Move $names to ${dest?.name}",
                    description = "Tap to proceed transition",
                )
            }
        }

        TYPE_COPY -> {
            if (value.size == 1) {
                TwoLineItem(
                    title = "Copy ${value.single().name} to ${dest?.name}",
                    description = "Tap to proceed transition",
                )
            } else {
                val names =
                    value
                        .take(5)
                        .joinToString { source -> source.name }

                TwoLineItem(
                    title = "Copy $names to ${dest?.name}",
                    description = "Tap to proceed transition",
                )
            }
        }

        else -> throw Throwable("Unsupported pending type ($type)")
    }

inline val Items.PENDING_TRANSITION: Int
    get() = 6
