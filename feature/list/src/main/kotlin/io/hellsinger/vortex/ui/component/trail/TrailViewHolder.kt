package io.hellsinger.vortex.ui.component.trail

import androidx.recyclerview.widget.RecyclerView

class TrailViewHolder(
    val root: TrailItemView,
) : RecyclerView.ViewHolder(root)
