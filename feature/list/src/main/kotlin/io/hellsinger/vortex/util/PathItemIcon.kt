package io.hellsinger.vortex.util

import android.graphics.drawable.Drawable
import io.hellsinger.filesystem.attribute.PathType
import io.hellsinger.filesystem.attribute.mimetype.MIME_TYPE_AUDIO
import io.hellsinger.filesystem.attribute.mimetype.MIME_TYPE_IMAGE
import io.hellsinger.filesystem.attribute.mimetype.MIME_TYPE_TEXT
import io.hellsinger.filesystem.attribute.mimetype.MIME_TYPE_VIDEO
import io.hellsinger.vortex.data.model.PathItem
import io.hellsinger.vortex.ui.icon.Icons

val PathItem.icon: Drawable?
    get() =
        when (type) {
            PathType.File ->
                when (mime.type) {
                    MIME_TYPE_VIDEO -> Icons.Rounded.Video
                    MIME_TYPE_IMAGE -> Icons.Rounded.Image
                    MIME_TYPE_AUDIO -> Icons.Rounded.Audio
                    MIME_TYPE_TEXT -> Icons.Rounded.Text
                    else -> Icons.Rounded.File
                }

            PathType.Directory -> Icons.Rounded.Directory
            PathType.Link -> Icons.Rounded.Link
            PathType.Pipe -> Icons.Rounded.Pipe
            PathType.Socket -> Icons.Rounded.Merge
            PathType.CharDevice -> Icons.Rounded.Merge
            PathType.BlockDevice -> Icons.Rounded.Publish
            else -> null
        }
