package io.hellsinger.vortex.ui.screen.storage.list

import android.os.Parcelable
import io.hellsinger.vortex.data.model.PathItem
import kotlinx.parcelize.Parcelize

@Parcelize
data class MutableUiState(
    override val items: MutableList<PathItem> = mutableListOf(),
    override val selected: MutableList<PathItem> = mutableListOf(),
    override var directoriesCount: Int = 0,
    override var filesCount: Int = 0,
    override val trail: MutableList<PathItem> = mutableListOf(),
    override var selectedTrailItemPosition: Int = -1,
) : Parcelable,
    UiState {
    override var selectedTrailItem: PathItem?
        get() = super.selectedTrailItem
        set(value) {
            trail[selectedTrailItemPosition] = value ?: return
        }

    fun replace(list: List<PathItem>) {
        items.clear()
        items.addAll(list)
    }
}
