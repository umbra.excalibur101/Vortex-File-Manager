package io.hellsinger.vortex.ui.component.dsl

import io.hellsinger.theme.vortex.VortexSettings
import io.hellsinger.vortex.R
import io.hellsinger.vortex.data.model.PathItem
import io.hellsinger.vortex.data.model.name
import io.hellsinger.vortex.ui.component.adapter.Item
import io.hellsinger.vortex.ui.component.adapter.divider
import io.hellsinger.vortex.ui.component.adapter.drawer
import io.hellsinger.vortex.ui.component.adapter.title
import io.hellsinger.vortex.ui.component.adapter.twoLine
import io.hellsinger.vortex.ui.icon.Icons

internal fun buildMenu(items: List<PathItem>) =
    if (items.size == 1) {
        buildSingleItemMenu(items[0])
    } else {
        buildMultipleMenu(items)
    }

private fun buildSingleItemMenu(item: PathItem): List<Item<*>> =
    buildList {
        title(VortexSettings.texts { key_storage_list_menu_primary_title })
        if (item.isDirectory) {
            drawer(
                R.id.file_manager_list_menu_op_open,
                VortexSettings.texts { key_storage_list_menu_operation_open },
                Icons.Rounded.FolderOpen,
            )
            drawer(
                R.id.file_manager_list_menu_op_add,
                VortexSettings.texts { key_storage_list_menu_operation_add_new },
                Icons.Rounded.FolderAdd,
            )
        }
        drawer(
            R.id.file_manager_list_menu_op_rename,
            VortexSettings.texts { key_storage_list_menu_operation_rename },
            Icons.Rounded.Edit,
        )
        drawer(
            R.id.file_manager_list_menu_op_delete,
            VortexSettings.texts { key_storage_list_menu_operation_delete },
            Icons.Rounded.Delete,
        )
        drawer(
            R.id.file_manager_list_menu_op_copy,
            VortexSettings.texts { key_storage_list_menu_operation_copy },
            Icons.Rounded.Copy,
        )
        drawer(
            R.id.file_manager_list_menu_op_move,
            VortexSettings.texts { key_storage_list_menu_operation_move },
            Icons.Rounded.FileMove,
        )
        divider()
        title(VortexSettings.texts { key_storage_list_menu_secondary_title })
        drawer(
            R.id.file_manager_list_menu_op_archive,
            VortexSettings.texts { key_storage_list_menu_operation_archive },
            Icons.Rounded.Archive,
        )
        drawer(
            R.id.file_manager_list_menu_op_copy_path,
            VortexSettings.texts { key_storage_list_menu_operation_copy_path },
            Icons.Rounded.Copy,
        )
        drawer(
            R.id.file_manager_list_menu_detail,
            VortexSettings.texts { key_storage_list_menu_operation_detail },
            Icons.Rounded.Info,
        )
        divider()
        twoLine(
            "Item",
            item.name,
        )
    }

private fun buildMultipleMenu(items: List<PathItem>): List<Item<*>> =
    buildList {
        title(VortexSettings.texts { key_storage_list_menu_primary_title })
        if (items.size == 2) {
            drawer(
                R.id.file_manager_list_menu_op_swap,
                VortexSettings.texts { key_storage_list_menu_operation_swap },
                Icons.Rounded.Swap,
            )
        }
        drawer(
            R.id.file_manager_list_menu_op_delete,
            VortexSettings.texts { key_storage_list_menu_operation_delete },
            Icons.Rounded.Delete,
        )
        drawer(
            R.id.file_manager_list_menu_op_copy,
            VortexSettings.texts { key_storage_list_menu_operation_copy },
            Icons.Rounded.Copy,
        )
        drawer(
            R.id.file_manager_list_menu_op_move,
            VortexSettings.texts { key_storage_list_menu_operation_move },
            Icons.Rounded.FileMove,
        )
        divider()
        title(VortexSettings.texts { key_storage_list_menu_secondary_title })
        drawer(
            R.id.file_manager_list_menu_op_archive,
            VortexSettings.texts { key_storage_list_menu_operation_archive },
            Icons.Rounded.Archive,
        )
        drawer(
            R.id.file_manager_list_menu_op_copy_path,
            VortexSettings.texts { key_storage_list_menu_operation_copy_path },
            Icons.Rounded.Copy,
        )
        divider()
        twoLine(
            "Items",
            items.take(5).joinToString { item ->
                item.name
            },
        )
    }
