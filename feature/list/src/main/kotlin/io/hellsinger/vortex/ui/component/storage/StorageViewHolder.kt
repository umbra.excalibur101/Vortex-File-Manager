package io.hellsinger.vortex.ui.component.storage

import androidx.recyclerview.widget.RecyclerView
import io.hellsinger.vortex.ui.component.storage.standard.linear.StandardStorageLinearCell

class StorageViewHolder(
    val root: StandardStorageLinearCell,
) : RecyclerView.ViewHolder(root)
