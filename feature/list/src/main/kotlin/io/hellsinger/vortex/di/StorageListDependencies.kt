package io.hellsinger.vortex.di

import android.content.Context
import io.hellsinger.vortex.Dispatchers
import io.hellsinger.vortex.data.repository.AndroidPermissions
import io.hellsinger.vortex.data.repository.AndroidRestrictedStorageRepository
import io.hellsinger.vortex.data.repository.StorageOperationStateConsumer
import io.hellsinger.vortex.domain.repository.PathObservation
import io.hellsinger.vortex.domain.repository.StorageRepository
import io.hellsinger.vortex.navigation.findDependencies
import io.hellsinger.vortex.ui.screen.storage.list.StorageListMenuTaskViewModel
import io.hellsinger.vortex.ui.screen.storage.list.StorageListScreenViewModel
import io.hellsinger.vortex.util.PathItemDescriptor
import io.hellsinger.vortex.util.StoragePathSegmentParser

interface StorageListDependencies {
    val repository: StorageRepository
    val dispatchers: Dispatchers
    val parser: StoragePathSegmentParser
    val permissions: AndroidPermissions
    val restricted: AndroidRestrictedStorageRepository
    val observation: PathObservation
    val consumer: StorageOperationStateConsumer
    val descriptor: PathItemDescriptor
}

internal fun Context.createListViewModel(): StorageListScreenViewModel =
    with(findDependencies<StorageListDependencies>()) {
        StorageListScreenViewModel(
            repository = repository,
            permissions = permissions,
            dispatchers = dispatchers,
            segment = parser,
            observation = observation,
            consumer = consumer,
            restricted = restricted,
        )
    }

internal fun Context.createListMenuTaskViewModel(): StorageListMenuTaskViewModel =
    with(findDependencies<StorageListDependencies>()) {
        StorageListMenuTaskViewModel(
            repository = repository,
        )
    }
