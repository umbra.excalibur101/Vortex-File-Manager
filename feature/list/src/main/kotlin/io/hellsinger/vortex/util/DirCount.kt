package io.hellsinger.vortex.util

import io.hellsinger.theme.vortex.VortexSettings
import io.hellsinger.theme.vortex.formatter

fun dirCount(count: Int) =
    when (count) {
        -1 -> VortexSettings.texts { key_storage_list_item_directory_zero_count }
        0 -> VortexSettings.texts { key_storage_list_item_directory_zero_count }
        1 -> VortexSettings.texts { key_storage_list_item_directory_single_count }
        else -> formatter(count) { key_storage_list_item_directory_multiple_count }
    }
