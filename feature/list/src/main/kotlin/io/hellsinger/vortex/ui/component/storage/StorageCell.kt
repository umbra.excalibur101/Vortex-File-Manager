package io.hellsinger.vortex.ui.component.storage

import android.graphics.drawable.Drawable

interface StorageCell {
    var title: String?

    var subtitle: String?

    var icon: Drawable?

    var flag: Int
}
