package io.hellsinger.vortex.ui.component.dsl

import android.view.ViewGroup
import io.hellsinger.vortex.ui.component.trail.TrailListView

fun ViewGroup.trail(
    index: Int,
    block: TrailListView.() -> Unit = {},
): TrailListView {
    val trail = TrailListView(context).apply(block)
    addView(trail.also(block), index)
    return trail
}
