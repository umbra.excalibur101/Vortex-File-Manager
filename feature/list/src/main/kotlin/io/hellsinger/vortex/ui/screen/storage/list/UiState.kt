package io.hellsinger.vortex.ui.screen.storage.list

import io.hellsinger.vortex.data.model.PathItem

interface UiState {
    val items: List<PathItem>
    val selected: List<PathItem>
    val directoriesCount: Int
    val filesCount: Int
    val trail: List<PathItem>
    val selectedTrailItemPosition: Int

    val selectedTrailItem: PathItem?
        get() = trail.getOrNull(selectedTrailItemPosition)
}
