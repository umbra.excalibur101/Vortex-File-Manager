package io.hellsinger.vortex.ui.screen.storage.create

interface UiState {
    var type: String
    val name: String
    val path: String
    val permission: String
}
