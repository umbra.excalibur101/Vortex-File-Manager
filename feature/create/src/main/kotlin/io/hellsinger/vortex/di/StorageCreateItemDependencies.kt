package io.hellsinger.vortex.di

import android.content.Context
import io.hellsinger.vortex.Dispatchers
import io.hellsinger.vortex.navigation.findDependencies
import io.hellsinger.vortex.ui.screen.storage.create.StorageCreateItemViewModel

interface StorageCreateItemDependencies {
    val dispatchers: Dispatchers
}

internal fun Context.createViewModel(): StorageCreateItemViewModel =
    with(findDependencies<StorageCreateItemDependencies>()) {
        StorageCreateItemViewModel(
            dispatchers = dispatchers,
        )
    }
