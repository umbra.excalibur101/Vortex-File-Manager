package io.hellsinger.vortex.ui.screen.storage.create

class MutableUiState(
    override var name: String = "",
    override var path: String = "",
    override var type: String = "",
    override var permission: String = "",
) : UiState
