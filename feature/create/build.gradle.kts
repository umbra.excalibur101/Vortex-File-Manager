plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.ktlint)
}

ktlint {
    android.set(true)
    outputColorName.set("RED")
}

android {
    namespace = "io.hellsinger.vortex"
    compileSdk = AndroidConfigure.targetSdk

    defaultConfig {
        minSdk = AndroidConfigure.minSdk

        consumerProguardFiles("consumer-rules.pro")
    }

    compileOptions {
        sourceCompatibility = BuildConfig.JDK.VerEnum
        targetCompatibility = BuildConfig.JDK.VerEnum
    }
}

dependencies {
    implementation(libs.androidx.core)
    implementation(libs.androidx.recycler)

    implementation(libs.kotlinx.coroutines.android)

    implementation(projects.theme)
    implementation(projects.theme.vortex)

    implementation(projects.filesystem)
    implementation(projects.filesystem.linux)

    implementation(projects.vortexFoundation)

    implementation(projects.core.data)
    implementation(projects.core.common)
    implementation(projects.core.domain)
    implementation(projects.core.viewmodel)
    implementation(projects.core.ui)
    implementation(projects.core.ui.icon)
    implementation(projects.core.navigation)
    implementation(projects.navigation)

    implementation(projects.feature.transaction)
}
