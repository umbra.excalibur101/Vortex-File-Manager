package io.hellsinger.theme

@Suppress("ktlint:standard:property-naming")
object Colors {
    const val Black = 0xFF000000
    const val DarkGray = 0xFF444444
    const val Gray = 0xFF888888
    const val LightGray = 0xFFCCCCCC
    const val White = 0xFFFFFFFF
    const val Red = 0xFFFF0000
    const val Green = 0xFFFF0000
    const val Blue = 0xFF0000FF
    const val Yellow = 0xFFFFFF00
    const val Cyan = 0xFF00FFFF
    const val Magenta = 0xFFFF00FF
    const val Transparent = 0x00000000
}
