package io.hellsinger.theme

@Suppress("ktlint:standard:property-naming")
object MaterialColors {
    object Red {
        const val color50: Long = 0xFFFFEBEE
        const val color100: Long = 0xFFFFCDD2
        const val color200: Long = 0xFFEF9A9A
        const val color300: Long = 0xFFE57373
        const val color400: Long = 0xFFEF5350
        const val color500: Long = 0xFFF44336
        const val color600: Long = 0xFFE53935
        const val color700: Long = 0xFFD32F2F
        const val color800: Long = 0xFFC62828
        const val color900: Long = 0xFFB71C1C

        const val colorA100: Long = 0xFFFF8A80
        const val colorA200: Long = 0xFFFF5252
        const val colorA400: Long = 0xFFFF1744
        const val colorA700: Long = 0xFFD50000
    }

    object Pink {
        const val color50: Long = 0xFFFCE4EC
        const val color100: Long = 0xFFF8BBD0
        const val color200: Long = 0xFFF48FB1
        const val color300: Long = 0xFFF06292
        const val color400: Long = 0xFFEC407A
        const val color500: Long = 0xFFE91E63
        const val color600: Long = 0xFFD81B60
        const val color700: Long = 0xFFC2185B
        const val color800: Long = 0xFFAD1457
        const val color900: Long = 0xFF880E4F

        const val colorA100: Long = 0xFFFF80AB
        const val colorA200: Long = 0xFFFF4081
        const val colorA400: Long = 0xFFF50057
        const val colorA700: Long = 0xFFC51162
    }

    object Purple {
        const val color50: Long = 0xFFF3E5F5
        const val color100: Long = 0xFFE1BEE7
        const val color200: Long = 0xFFCE93D8
        const val color300: Long = 0xFFBA68C8
        const val color400: Long = 0xFFAB47BC
        const val color500: Long = 0xFF9C27B0
        const val color600: Long = 0xFF8E24AA
        const val color700: Long = 0xFF7B1FA2
        const val color800: Long = 0xFF6A1B9A
        const val color900: Long = 0xFF4A148C

        const val colorA100: Long = 0xFFEA80FC
        const val colorA200: Long = 0xFFE040FB
        const val colorA400: Long = 0xFFD500F9
        const val colorA700: Long = 0xFFAA00FF
    }

    object DeepPurple {
        const val color50: Long = 0xFFEDE7F6
        const val color100: Long = 0xFFD1C4E9
        const val color200: Long = 0xFFB39DDB
        const val color300: Long = 0xFF9575CD
        const val color400: Long = 0xFF7E57C2
        const val color500: Long = 0xFF673AB7
        const val color600: Long = 0xFF5E35B1
        const val color700: Long = 0xFF512DA8
        const val color800: Long = 0xFF4527A0
        const val color900: Long = 0xFF311B92

        const val colorA100: Long = 0xFFB388FF
        const val colorA200: Long = 0xFF7C4DFF
        const val colorA400: Long = 0xFF651FFF
        const val colorA700: Long = 0xFF6200EA
    }

    object Indigo {
        const val color50: Long = 0xFFE8EAF6
        const val color100: Long = 0xFFC5CAE9
        const val color200: Long = 0xFF9FA8DA
        const val color300: Long = 0xFF7986CB
        const val color400: Long = 0xFF5C6BC0
        const val color500: Long = 0xFF3F51B5
        const val color600: Long = 0xFF3949AB
        const val color700: Long = 0xFF303F9F
        const val color800: Long = 0xFF283593
        const val color900: Long = 0xFF1A237E

        const val colorA100: Long = 0xFF8C9EFF
        const val colorA200: Long = 0xFF536DFE
        const val colorA400: Long = 0xFF3D5AFE
        const val colorA700: Long = 0xFF304FFE
    }

    object Blue {
        const val color50: Long = 0xFFE3F2FD
        const val color100: Long = 0xFFBBDEFB
        const val color200: Long = 0xFF90CAF9
        const val color300: Long = 0xFF64B5F6
        const val color400: Long = 0xFF42A5F5
        const val color500: Long = 0xFF2196F3
        const val color600: Long = 0xFF1E88E5
        const val color700: Long = 0xFF1976D2
        const val color800: Long = 0xFF1565C0
        const val color900: Long = 0xFF0D47A1

        const val colorA100: Long = 0xFF82B1FF
        const val colorA200: Long = 0xFF448AFF
        const val colorA400: Long = 0xFF2979FF
        const val colorA700: Long = 0xFF2962FF
    }

    object LightBlue {
        const val color50: Long = 0xFFE1F5FE
        const val color100: Long = 0xFFB3E5FC
        const val color200: Long = 0xFF81D4FA
        const val color300: Long = 0xFF4FC3F7
        const val color400: Long = 0xFF29B6F6
        const val color500: Long = 0xFF03A9F4
        const val color600: Long = 0xFF039BE5
        const val color700: Long = 0xFF0288D1
        const val color800: Long = 0xFF0277BD
        const val color900: Long = 0xFF0D47A1

        const val colorA100: Long = 0xFF82B1FF
        const val colorA200: Long = 0xFF448AFF
        const val colorA400: Long = 0xFF2979FF
        const val colorA700: Long = 0xFF2962FF
    }

    object Cyan {
        const val color50: Long = 0xFFE0F7FA
        const val color100: Long = 0xFFB2EBF2
        const val color200: Long = 0xFF80DEEA
        const val color300: Long = 0xFF4DD0E1
        const val color400: Long = 0xFF26C6DA
        const val color500: Long = 0xFF00BCD4
        const val color600: Long = 0xFF00ACC1
        const val color700: Long = 0xFF0097A7
        const val color800: Long = 0xFF00838F
        const val color900: Long = 0xFF006064

        const val colorA100: Long = 0xFF84FFFF
        const val colorA200: Long = 0xFF18FFFF
        const val colorA400: Long = 0xFF00E5FF
        const val colorA700: Long = 0xFF00B8D4
    }

    object Teal {
        const val color50: Long = 0xFFE0F2F1
        const val color100: Long = 0xFFB2DFDB
        const val color200: Long = 0xFF80CBC4
        const val color300: Long = 0xFF4DB6AC
        const val color400: Long = 0xFF26A69A
        const val color500: Long = 0xFF009688
        const val color600: Long = 0xFF00897B
        const val color700: Long = 0xFF00796B
        const val color800: Long = 0xFF00695C
        const val color900: Long = 0xFF004D40

        const val colorA100: Long = 0xFFA7FFEB
        const val colorA200: Long = 0xFF64FFDA
        const val colorA400: Long = 0xFF1DE9B6
        const val colorA700: Long = 0xFF1DE9B6
    }
}
