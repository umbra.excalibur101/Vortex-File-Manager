package io.hellsinger.theme

interface ThemeFile {
    suspend fun read(name: String)

    suspend fun write(name: String)
}
