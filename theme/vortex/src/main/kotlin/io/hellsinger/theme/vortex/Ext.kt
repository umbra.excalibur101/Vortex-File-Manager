package io.hellsinger.theme.vortex

inline fun formatter(
    vararg args: Any?,
    picker: VortexTextsKeys.() -> Int,
): String = VortexSettings.texts(picker = picker).format(args = args)
