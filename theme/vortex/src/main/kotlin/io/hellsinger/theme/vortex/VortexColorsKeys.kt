@file:Suppress("ktlint")


package io.hellsinger.theme.vortex

import androidx.annotation.ColorInt
import androidx.annotation.ColorLong

@Suppress("PropertyName")
class VortexColorsKeys : VortexKeys<Int>, Iterable<Int> {
    var colorCount = 0
        private set

    // Navigation Bar
    val key_navigation_bar_surface_color = colorCount++
    val key_navigation_bar_title_color = colorCount++
    val key_navigation_bar_subtitle_color = colorCount++
    val key_navigation_bar_icon_color = colorCount++
    val key_navigation_bar_icon_ripple_color = colorCount++

    // Navigation Drawer
    val key_navigation_drawer_background_color = colorCount++
    val key_navigation_drawer_title_color = colorCount++
    val key_navigation_drawer_item_surface_color = colorCount++
    val key_navigation_drawer_item_icon_color = colorCount++
    val key_navigation_drawer_item_title_color = colorCount++
    val key_navigation_drawer_item_ripple_color = colorCount++

    // Storage List
    var key_storage_list_background_color = colorCount++

    val key_storage_list_item_surface_color = colorCount++
    val key_storage_list_item_surface_ripple_color = colorCount++
    val key_storage_list_item_title_color = colorCount++
    val key_storage_list_item_subtitle_color = colorCount++
    val key_storage_list_item_icon_color = colorCount++
    val key_storage_list_item_icon_surface_color = colorCount++
    val key_storage_list_item_selected_color = colorCount++

    val key_storage_list_trail_surface_color = colorCount++
    val key_storage_list_trail_item_text_color = colorCount++
    val key_storage_list_trail_item_arrow_color = colorCount++
    val key_storage_list_trail_item_ripple_color = colorCount++
    val key_storage_list_trail_item_text_selected_color = colorCount++
    val key_storage_list_trail_item_arrow_selected_color = colorCount++
    val key_storage_list_trail_item_ripple_selected_color = colorCount++

    val key_storage_list_loading_title_color = colorCount++
    val key_storage_list_loading_progress_color = colorCount++

    val key_storage_list_info_message_color = colorCount++
    val key_storage_list_info_icon_color = colorCount++

    val key_storage_list_menu_background_color = colorCount++
    val key_storage_list_menu_title_color = colorCount++
    val key_storage_list_menu_drawer_surface_color = colorCount++
    val key_storage_list_menu_drawer_icon_color = colorCount++
    val key_storage_list_menu_drawer_title_color = colorCount++
    val key_storage_list_menu_drawer_ripple_color = colorCount++
    val key_storage_list_menu_divider_color = colorCount++
    val key_storage_list_menu_two_line_surface_color = colorCount++
    val key_storage_list_menu_two_line_title_color = colorCount++
    val key_storage_list_menu_two_line_description_color = colorCount++
    val key_storage_list_menu_two_line_ripple_color = colorCount++

    val key_storage_list_menu_task_background_color = colorCount++
    val key_storage_list_menu_task_title_color = colorCount++
    val key_storage_list_menu_task_item_surface_color = colorCount++
    val key_storage_list_menu_task_item_title_color = colorCount++
    val key_storage_list_menu_task_item_description_color = colorCount++
    val key_storage_list_menu_task_item_ripple_color = colorCount++

    // Storage Create Item
    val key_storage_create_item_background_color = colorCount++
    val key_storage_create_item_field_cursor_color = colorCount++
    val key_storage_create_item_field_line_color = colorCount++
    val key_storage_create_item_field_hint_color = colorCount++
    val key_storage_create_item_field_text_color = colorCount++

    // Storage Transaction
    val key_storage_transaction_background_color = colorCount++
    val key_storage_transaction_title_color = colorCount++
    val key_storage_transaction_two_line_surface_color = colorCount++
    val key_storage_transaction_two_line_title_color = colorCount++
    val key_storage_transaction_two_line_description_color = colorCount++
    val key_storage_transaction_two_line_ripple_color = colorCount++

    private val colors = IntArray(colorCount)

    override operator fun set(index: Int, @ColorInt value: Int) {
        colors[index] = value
    }

    operator fun set(index: Int, @ColorLong color: Long) = set(index, color.toInt())

    override fun iterator(): Iterator<Int> = colors.iterator()

    override operator fun get(index: Int): Int = colors[index]

}
