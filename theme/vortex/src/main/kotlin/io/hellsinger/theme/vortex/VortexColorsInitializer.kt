@file:Suppress("ktlint")

package io.hellsinger.theme.vortex

import io.hellsinger.theme.Colors

private inline val Colors.Accent
    get() = 0xFF6687FF

private inline val Colors.BlurAccent
    get() = 0x326687FF

fun VortexColorsKeys.initialize(isDarkMode: Boolean) {
    if (isDarkMode) initDarkMode() else initLightMode()
}

private fun VortexColorsKeys.initDarkMode() {
    set(key_navigation_bar_surface_color, 0xFF333333)
    set(key_navigation_bar_title_color, Colors.White)
    set(key_navigation_bar_subtitle_color, Colors.White)
    set(key_navigation_bar_icon_color, Colors.White)
    set(key_navigation_bar_icon_ripple_color, 0x32FFFFFF)

    set(key_navigation_drawer_background_color, 0xFF212121)
    set(key_navigation_drawer_title_color, Colors.White)
    set(key_navigation_drawer_item_surface_color, 0xFF212121)
    set(key_navigation_drawer_item_icon_color, Colors.White)
    set(key_navigation_drawer_item_title_color, Colors.White)
    set(key_navigation_drawer_item_ripple_color, 0x32FFFFFF)

    set(key_storage_list_background_color, 0xFF121212)
    set(key_storage_list_item_surface_color, 0xFF212121)
    set(key_storage_list_item_surface_ripple_color, 0x32FFFFFF)
    set(key_storage_list_item_title_color, Colors.White)
    set(key_storage_list_item_subtitle_color, Colors.White)
    set(key_storage_list_item_icon_color, Colors.White)
    set(key_storage_list_item_icon_surface_color, Colors.DarkGray)
    set(key_storage_list_item_selected_color, Colors.Accent)

    set(key_storage_list_trail_surface_color, 0xFF333333)
    set(key_storage_list_trail_item_text_color, Colors.White)
    set(key_storage_list_trail_item_arrow_color, Colors.White)
    set(key_storage_list_trail_item_ripple_color, 0x32FFFFFF)
    set(key_storage_list_trail_item_text_selected_color, Colors.Accent)
    set(key_storage_list_trail_item_arrow_selected_color, Colors.Accent)
    set(key_storage_list_trail_item_ripple_selected_color, Colors.BlurAccent)

    set(key_storage_list_loading_title_color, Colors.White)
    set(key_storage_list_loading_progress_color, Colors.White)

    set(key_storage_list_info_message_color, Colors.White)
    set(key_storage_list_info_icon_color, Colors.White)

    set(key_storage_list_menu_background_color, 0xFF212121)
    set(key_storage_list_menu_title_color, Colors.White)
    set(key_storage_list_menu_drawer_surface_color, 0xFF212121)
    set(key_storage_list_menu_drawer_icon_color, Colors.White)
    set(key_storage_list_menu_drawer_title_color, Colors.White)
    set(key_storage_list_menu_drawer_ripple_color, 0x32FFFFFF)
    set(key_storage_list_menu_divider_color, Colors.LightGray)
    set(key_storage_list_menu_two_line_surface_color, 0xFF212121)
    set(key_storage_list_menu_two_line_title_color, Colors.White)
    set(key_storage_list_menu_two_line_description_color, Colors.LightGray)
    set(key_storage_list_menu_two_line_ripple_color, 0x32FFFFFF)

    set(key_storage_list_menu_task_background_color, 0xFF212121)
    set(key_storage_list_menu_task_title_color, Colors.White)
    set(key_storage_list_menu_task_item_surface_color, 0xFF212121)
    set(key_storage_list_menu_task_item_title_color, Colors.White)
    set(key_storage_list_menu_task_item_description_color, Colors.LightGray)
    set(key_storage_list_menu_task_item_ripple_color, 0x32FFFFFF)

    set(key_storage_create_item_background_color, 0xFF121212)
    set(key_storage_create_item_field_cursor_color, Colors.Accent)
    set(key_storage_create_item_field_line_color, Colors.Accent)
    set(key_storage_create_item_field_hint_color, Colors.LightGray)
    set(key_storage_create_item_field_text_color, Colors.White)

    set(key_storage_transaction_background_color, 0xFF121212)
    set(key_storage_transaction_title_color, Colors.White)
    set(key_storage_transaction_two_line_surface_color, 0xFF212121)
    set(key_storage_transaction_two_line_title_color, Colors.White)
    set(key_storage_transaction_two_line_description_color, Colors.LightGray)
    set(key_storage_transaction_two_line_ripple_color, 0x32FFFFFF)
}

private fun VortexColorsKeys.initLightMode() {

}