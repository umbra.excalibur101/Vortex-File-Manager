package io.hellsinger.theme.vortex

interface VortexKeys<T> {
    operator fun set(
        index: Int,
        value: T,
    )

    operator fun get(index: Int): T
}
