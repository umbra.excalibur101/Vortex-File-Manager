package io.hellsinger.theme.vortex

private const val INTEGER_SPECIFIER = "%d"
private const val STRING_SPECIFIER = "%s"

fun VortexTextsKeys.initialize(isSiSizeApply: Boolean = false) {
    set(key_navigation_drawer_title, "Vortex")
    set(key_navigation_drawer_item_file_manager, "File Manager")
    set(key_navigation_drawer_item_settings, "Settings")

    set(key_storage_path_item_name_external_storage, "Internal storage")

    set(key_storage_path_item_mime_type_application, "Application")
    set(key_storage_path_item_mime_type_image, "Image")
    set(key_storage_path_item_mime_type_video, "Video")
    set(key_storage_path_item_mime_type_audio, "Audio")
    set(key_storage_path_item_mime_type_text, "Text")

    if (isSiSizeApply) {
        applySiSizeUnits()
    } else {
        applyIECSizeUnits()
    }

    set(key_storage_list_info_message_empty, "$STRING_SPECIFIER is empty")
    set(key_storage_list_info_message_restricted, "$STRING_SPECIFIER is restricted")
    set(
        key_storage_list_info_message_access,
        "App requires full storage access.\n" +
            "Full storage access gives app full control of your file-system storage.\n" +
            "App guaranties your files can't be damaged.\n" +
            "Remember: Plugins can disallow this rule.",
    )
    set(
        key_storage_list_info_message_full_access,
        "App requires storage access.\n" +
            "Storage access gives app full control of your file-system storage.\n" +
            "App guaranties your files can't be damaged.\n" +
            "Remember: Plugins can disallow this rule.\n",
    )

    set(key_storage_list_item_description_separator, " | ")

    set(key_storage_list_item_description_placeholder, "Getting content...")
    set(key_storage_list_item_description_restricted, "Restricted")

    set(key_storage_list_item_directory_multiple_count, "$INTEGER_SPECIFIER items")
    set(key_storage_list_item_directory_single_count, "Single item")
    set(key_storage_list_item_directory_zero_count, "Empty")

    set(key_storage_list_navigation_action_provide_access, "Provide access")
    set(key_storage_list_navigation_action_provide_full_access, "Provide full access")
    set(key_storage_list_navigation_action_search, "Search")
    set(key_storage_list_navigation_action_more, "More")
    set(key_storage_list_navigation_action_tasks, "Tasks")

    set(key_storage_list_menu_primary_title, "Primary")
    set(key_storage_list_menu_secondary_title, "Miscellaneous")
    set(key_storage_list_menu_operation_open, "Open")
    set(key_storage_list_menu_operation_add_new, "Add new")
    set(key_storage_list_menu_operation_delete, "Delete")
    set(key_storage_list_menu_operation_rename, "Rename")
    set(key_storage_list_menu_operation_copy, "Copy")
    set(key_storage_list_menu_operation_move, "Move")
    set(key_storage_list_menu_operation_detail, "Detail")
    set(key_storage_list_menu_operation_swap, "Detail")
    set(key_storage_list_menu_operation_archive, "Archive")
    set(key_storage_list_menu_operation_copy_path, "Copy path")

    set(key_storage_service_item_deleted_message, "$STRING_SPECIFIER deleted")
    set(key_storage_service_item_created_message, "$STRING_SPECIFIER create as $STRING_SPECIFIER")

    set(
        key_storage_service_vortex_channel_description,
        "Vortex Service channel is main notification channel of service. It's about changed inside service work or behavior",
    )

    set(
        key_storage_service_operation_channel_description,
        "Transaction channel describes what operations happens in foreground Vortex Service",
    )
}

private fun VortexTextsKeys.applySiSizeUnits() {
    set(key_storage_path_item_size_byte, "B")
    set(key_storage_path_item_size_kilo_byte, "KB")
    set(key_storage_path_item_size_mega_byte, "MB")
    set(key_storage_path_item_size_giga_byte, "GB")
    set(key_storage_path_item_size_tera_byte, "TB")
    set(key_storage_path_item_size_peta_byte, "PB")
    set(key_storage_path_item_size_ebi_byte, "EB")
    set(key_storage_path_item_size_zetta_byte, "ZB")
    set(key_storage_path_item_size_yotta_byte, "YB")
}

private fun VortexTextsKeys.applyIECSizeUnits() {
    set(key_storage_path_item_size_byte, "B")
    set(key_storage_path_item_size_kilo_byte, "KiB")
    set(key_storage_path_item_size_mega_byte, "MiB")
    set(key_storage_path_item_size_giga_byte, "GiB")
    set(key_storage_path_item_size_tera_byte, "TiB")
    set(key_storage_path_item_size_peta_byte, "PiB")
    set(key_storage_path_item_size_ebi_byte, "EiB")
    set(key_storage_path_item_size_zetta_byte, "ZiB")
    set(key_storage_path_item_size_yotta_byte, "YiB")
}
