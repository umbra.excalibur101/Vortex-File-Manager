package io.hellsinger.theme.vortex

object VortexSettings {
    val colors = VortexColorsKeys()
    val texts = VortexTextsKeys()

    inline fun colors(picker: VortexColorsKeys.() -> Int): Int = colors[picker(colors)]

    inline fun texts(picker: VortexTextsKeys.() -> Int): String = texts[picker(texts)]
}
