@file:Suppress("ktlint", )

package io.hellsinger.theme.vortex

@Suppress("PropertyName")
class VortexTextsKeys : VortexKeys<String>, Iterable<String?> {
    var textCount = 0
        private set

    val key_navigation_drawer_title = textCount++
    val key_navigation_drawer_item_file_manager = textCount++
    val key_navigation_drawer_item_settings = textCount++

    val key_storage_path_item_name_external_storage = textCount++

    val key_storage_path_item_mime_type_application = textCount++
    val key_storage_path_item_mime_type_image = textCount++
    val key_storage_path_item_mime_type_video = textCount++
    val key_storage_path_item_mime_type_audio = textCount++
    val key_storage_path_item_mime_type_text = textCount++

    val key_storage_path_item_size_byte = textCount++
    val key_storage_path_item_size_kilo_byte = textCount++
    val key_storage_path_item_size_mega_byte = textCount++
    val key_storage_path_item_size_giga_byte = textCount++
    val key_storage_path_item_size_tera_byte = textCount++
    val key_storage_path_item_size_peta_byte = textCount++
    val key_storage_path_item_size_ebi_byte = textCount++
    val key_storage_path_item_size_zetta_byte = textCount++
    val key_storage_path_item_size_yotta_byte = textCount++

    val key_storage_list_info_message_empty = textCount++
    val key_storage_list_info_message_restricted = textCount++
    val key_storage_list_info_message_access = textCount++
    val key_storage_list_info_message_full_access = textCount++

    val key_storage_list_item_description_separator = textCount++
    val key_storage_list_item_description_placeholder = textCount++
    val key_storage_list_item_description_restricted = textCount++

    val key_storage_list_item_directory_multiple_count = textCount++
    val key_storage_list_item_directory_single_count = textCount++
    val key_storage_list_item_directory_zero_count = textCount++

    val key_storage_list_navigation_action_provide_access = textCount++
    val key_storage_list_navigation_action_provide_full_access = textCount++
    val key_storage_list_navigation_action_search = textCount++
    val key_storage_list_navigation_action_more = textCount++
    val key_storage_list_navigation_action_tasks = textCount++

    val key_storage_list_menu_primary_title = textCount++
    val key_storage_list_menu_secondary_title = textCount++
    val key_storage_list_menu_operation_open = textCount++
    val key_storage_list_menu_operation_add_new = textCount++
    val key_storage_list_menu_operation_delete = textCount++
    val key_storage_list_menu_operation_rename = textCount++
    val key_storage_list_menu_operation_copy = textCount++
    val key_storage_list_menu_operation_move = textCount++
    val key_storage_list_menu_operation_detail = textCount++
    val key_storage_list_menu_operation_swap = textCount++
    val key_storage_list_menu_operation_archive = textCount++
    val key_storage_list_menu_operation_copy_path = textCount++

    val key_storage_service_item_deleted_message = textCount++
    val key_storage_service_item_created_message = textCount++

    val key_storage_service_vortex_channel_description = textCount++
    val key_storage_service_operation_channel_description = textCount++

    private val texts = arrayOfNulls<String>(textCount)

    override fun set(index: Int, value: String) {
        texts[index] = value
    }

    override fun iterator(): Iterator<String?> = texts.iterator()

    override fun get(index: Int): String = texts[index]!!
}
