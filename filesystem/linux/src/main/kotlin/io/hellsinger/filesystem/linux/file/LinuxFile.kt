package io.hellsinger.filesystem.linux.file

import io.hellsinger.filesystem.error.PathDoesNotExist
import io.hellsinger.filesystem.file.File
import io.hellsinger.filesystem.linux.LinuxFileOperationProvider
import io.hellsinger.filesystem.linux.attribute.LinuxPathAttributeImpl
import io.hellsinger.filesystem.linux.directory.LinuxDirectoryEntry
import io.hellsinger.filesystem.path.Path
import io.hellsinger.filesystem.path.PathOwner
import io.hellsinger.filesystem.size.SiSize
import io.hellsinger.filesystem.size.Size

interface LinuxFile<P : Path> : File<P> {
    override val size: Size

    val mode: Int
}

private class LinuxFileImpl<P : Path>(
    override val path: P,
    attr: LinuxPathAttributeImpl =
        LinuxFileOperationProvider.getStatus(path.bytes)
            ?: PathDoesNotExist(path),
) : LinuxFile<P> {
    override val size: Size = SiSize(attr.size)
    override val mode: Int = attr.mode
}

fun <P : Path> P.asLinuxFile(): LinuxFile<P> = LinuxFileImpl(this)

fun <P : Path> PathOwner<P>.asLinuxFile(): LinuxFile<P> = LinuxFileImpl(path)

fun <P : Path> LinuxDirectoryEntry<P>.asLinuxFile(): LinuxFile<P> = LinuxFileImpl(path)
