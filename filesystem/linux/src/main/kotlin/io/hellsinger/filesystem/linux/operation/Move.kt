package io.hellsinger.filesystem.linux.operation

import io.hellsinger.filesystem.path.Path

fun <P : Path> P.move(dest: P) = rename(dest)
