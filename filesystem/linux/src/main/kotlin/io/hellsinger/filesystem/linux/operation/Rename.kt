package io.hellsinger.filesystem.linux.operation

import io.hellsinger.filesystem.checkScheme
import io.hellsinger.filesystem.linux.LinuxFileOperationProvider
import io.hellsinger.filesystem.linux.LinuxOperationOptions
import io.hellsinger.filesystem.path.Path

fun <P : Path> P.rename(dest: P) {
    checkScheme(LinuxOperationOptions.SCHEME)
    dest.checkScheme(LinuxOperationOptions.SCHEME)
    LinuxFileOperationProvider.rename(bytes, dest.bytes)
}
