package io.hellsinger.filesystem.linux.error

class ErrnoException internal constructor(
    val name: String,
    val number: Int,
    val desc: String,
) : Throwable("An error occurred at native method (name: $name, error number: $number, description: $desc)")

// TODO: Replace ErrnoException with this fun to hide impl
fun ErrnoException.rethrow(): Nothing = throw Throwable("$name: $desc")
