package io.hellsinger.filesystem.linux.directory

import io.hellsinger.filesystem.attribute.PathType
import io.hellsinger.filesystem.directory.DirectoryEntry
import io.hellsinger.filesystem.path.Path

interface LinuxDirectoryEntry<P : Path> : DirectoryEntry<P> {
    val id: Long
    val type: PathType
}
