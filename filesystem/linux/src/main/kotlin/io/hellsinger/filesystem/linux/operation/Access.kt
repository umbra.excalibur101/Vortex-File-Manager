package io.hellsinger.filesystem.linux.operation

import io.hellsinger.filesystem.linux.LinuxFileOperationProvider
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Access
import io.hellsinger.filesystem.path.Path
import io.hellsinger.filesystem.path.PathOwner

fun <P : Path> P.access(mode: Int) = LinuxFileOperationProvider.access(bytes, mode)

inline val <P : Path> P.writeable
    get() = access(Access.WRITABLE) == 0

inline val <P : Path> PathOwner<P>.writeable
    get() = path.access(Access.WRITABLE) == 0
