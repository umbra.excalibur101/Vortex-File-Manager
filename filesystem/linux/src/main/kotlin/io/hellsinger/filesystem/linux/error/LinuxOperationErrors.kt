package io.hellsinger.filesystem.linux.error

object LinuxOperationErrors {
    const val E_PERM = 1
    const val E_NOENT = 2
    const val E_SRCH = 3
    const val E_INTR = 4
    const val E_IO = 5
    const val E_NXIO = 6
    const val E_2BIG = 7
    const val E_NOEXEC = 8
    const val E_BADF = 9
    const val E_CHILD = 10
    const val E_AGAIN = 11
    const val E_NOMEM = 12
    const val E_ACCES = 13
    const val E_FAULT = 14
    const val E_NOTBLK = 15
    const val E_BUSY = 16
    const val E_EXIST = 17
    const val E_XDEV = 18
    const val E_NODEV = 19
    const val E_NOTDIR = 20
    const val E_ISDIR = 21
    const val E_INVAL = 22
    const val E_NFILE = 23
    const val E_MFILE = 24
    const val E_NOTTY = 25
    const val E_TXTBSY = 26
    const val E_FBIG = 27
    const val E_NOSPC = 28
    const val E_SPIPE = 29
    const val E_ROFS = 30
    const val E_MLINK = 31
    const val E_PIPE = 32
    const val E_DOM = 33
    const val E_RANGE = 34
}
