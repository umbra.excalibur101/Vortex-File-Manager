package io.hellsinger.filesystem.linux.attribute

import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.EXECUTE_GROUP
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.EXECUTE_OTHER
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.EXECUTE_USER
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.READ_GROUP
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.READ_OTHER
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.READ_USER
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.WRITE_GROUP
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.WRITE_OTHER
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.WRITE_USER

fun Int.toLinuxEntryModeString() =
    buildString(capacity = 9) {
        if (and(READ_USER) != 0) append('r') else append('-')
        if (and(WRITE_USER) != 0) append('w') else append('-')
        if (and(EXECUTE_USER) != 0) append('x') else append('-')
        if (and(READ_GROUP) != 0) append('r') else append('-')
        if (and(WRITE_GROUP) != 0) append('w') else append('-')
        if (and(EXECUTE_GROUP) != 0) append('x') else append('-')
        if (and(READ_OTHER) != 0) append('r') else append('-')
        if (and(WRITE_OTHER) != 0) append('w') else append('-')
        if (and(EXECUTE_OTHER) != 0) append('x') else append('-')
    }
