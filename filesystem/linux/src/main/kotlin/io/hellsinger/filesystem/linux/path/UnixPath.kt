package io.hellsinger.filesystem.linux.path

import io.hellsinger.filesystem.path.Path
import io.hellsinger.filesystem.path.PathWrapper

@Suppress("FunctionName")
fun UnixPath(bytes: ByteArray): Path =
    object : PathWrapper(bytes) {
        override val separator: Byte = '/'.code.toByte()

        init {
            bytes.forEachIndexed { index, part ->
                if (part == separator) separators.add(index)
            }
        }

        override val isAbsolute: Boolean
            get() = !isEmpty && bytes[0] == separator

        override val isHidden: Boolean
            get() = isAbsolute && startsWith(onCreatePath(byteArrayOf('.'.code.toByte())))

        override val scheme: Int = io.hellsinger.filesystem.linux.LinuxOperationOptions.SCHEME

        override fun onCreatePath(path: ByteArray): Path = UnixPath(path)
    }

@Suppress("FunctionName")
fun LinuxPath(bytes: ByteArray) = UnixPath(bytes)
