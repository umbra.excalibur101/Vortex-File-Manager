package io.hellsinger.filesystem.linux.operation

import io.hellsinger.filesystem.linux.LinuxFileOperationProvider
import io.hellsinger.filesystem.path.Path

fun <P : Path> P.createSymbolicLink(path: Path) {
    LinuxFileOperationProvider.createSymbolicLink(bytes, path.bytes)
}

fun <P : Path> P.createLink(path: Path) {
}
