package io.hellsinger.filesystem.linux.attribute

import io.hellsinger.filesystem.attribute.PathGroup
import io.hellsinger.filesystem.linux.LinuxFileOperationProvider

interface LinuxGroup : PathGroup {
    override val name: ByteArray
    val password: ByteArray
    override val id: Int
}

internal class LinuxGroupEntryImpl(
    override val id: Int,
    override val name: ByteArray,
    override val password: ByteArray,
) : LinuxGroup

fun getLinuxGroup(id: Int): LinuxGroup = LinuxFileOperationProvider.getGroupEntry(id)

val LinuxPathAttribute.group: LinuxGroup
    get() = getLinuxGroup(groupId)
