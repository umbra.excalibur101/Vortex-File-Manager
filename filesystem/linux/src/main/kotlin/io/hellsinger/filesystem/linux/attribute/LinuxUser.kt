package io.hellsinger.filesystem.linux.attribute

import io.hellsinger.filesystem.attribute.PathUser
import io.hellsinger.filesystem.linux.LinuxFileOperationProvider

interface LinuxUser : PathUser {
    override val name: ByteArray
    val password: ByteArray
    override val id: Int
    val home: ByteArray
    val shell: ByteArray
}

internal class LinuxUserEntryImpl(
    override val id: Int,
    override val name: ByteArray,
    override val password: ByteArray,
    override val home: ByteArray,
    override val shell: ByteArray,
) : LinuxUser

fun getLinuxUser(id: Int): LinuxUser = LinuxFileOperationProvider.getUserEntry(id)

val LinuxPathAttribute.user: LinuxUser
    get() = getLinuxUser(userId)
