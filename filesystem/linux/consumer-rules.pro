-keepclasseswithmembernames,includedescriptorclasses class * {
    native <methods>;
}

-keep class io.hellsinger.filesystem.linux.attribute.LinuxPathAttributeImpl { *;}
-keep class io.hellsinger.filesystem.linux.attribute.LinuxFileSystemAttributeImpl { *;}
-keep class io.hellsinger.filesystem.linux.attribute.LinuxUserEntryImpl { *; }
-keep class io.hellsinger.filesystem.linux.attribute.LinuxGroupEntryImpl { *;}
-keep class io.hellsinger.filesystem.linux.directory.LinuxDirectoryEntryImpl { *;}
-keep class io.hellsinger.filesystem.linux.operation.LinuxPathObserverImpl$LinuxObservablePathImpl { *;}
-keep class io.hellsinger.filesystem.linux.error.ErrnoException