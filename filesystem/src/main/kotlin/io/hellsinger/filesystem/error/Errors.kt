package io.hellsinger.filesystem.error

import io.hellsinger.filesystem.path.Path

fun PathDoesNotExist(path: Path): Nothing = throw Throwable("$path does not exist")

fun FileOpenError(path: Path): Nothing = throw Throwable("Couldn't open $path")
