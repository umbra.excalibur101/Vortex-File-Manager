package io.hellsinger.filesystem.attribute

interface PathUser {
    val id: Int
    val name: ByteArray
}
