package io.hellsinger.filesystem.attribute

@JvmInline
value class PathType(
    val original: Int,
) {
    companion object {
        val Unknown = PathType(original = -1)
        val File = PathType(original = 0)
        val Directory = PathType(original = 1)
        val Link = PathType(original = 2)
        val Pipe = PathType(original = 3)
        val Socket = PathType(original = 4)
        val BlockDevice = PathType(original = 5)
        val CharDevice = PathType(original = 6)

        // for `stat64` structure
        private const val S_IFIFO = 4096
        private const val S_IFCHR = 8192
        private const val S_IFDIR = 16384
        private const val S_IFBLK = 24576
        private const val S_IFREG = 32768
        private const val S_IFLNK = 40960
        private const val S_IFSOCK = 49152

        // for `dirent` structure
        private const val DT_FIFO = 1
        private const val DT_CHR = 2
        private const val DT_DIR = 4
        private const val DT_BLK = 6
        private const val DT_REG = 8
        private const val DT_LNK = 10
        private const val DT_SOCK = 12

        fun fromStat(original: Int): PathType =
            when (original) {
                S_IFDIR -> Directory
                S_IFLNK -> Link
                S_IFREG -> File
                S_IFIFO -> Pipe
                S_IFSOCK -> Socket
                S_IFBLK -> BlockDevice
                S_IFCHR -> CharDevice
                else -> Unknown
            }

        fun fromDirent(original: Int): PathType =
            when (original) {
                DT_LNK -> Link
                DT_DIR -> Directory
                DT_BLK -> BlockDevice
                DT_SOCK -> Socket
                DT_REG -> File
                DT_CHR -> CharDevice
                DT_FIFO -> Pipe
                else -> Unknown
            }
    }
}
