package io.hellsinger.filesystem.attribute.mimetype

import io.hellsinger.filesystem.path.Path

const val MIME_TYPE_EMPTY = -1
const val MIME_TYPE_APPLICATION = 0
const val MIME_TYPE_AUDIO = 1
const val MIME_TYPE_IMAGE = 2
const val MIME_TYPE_TEXT = 4
const val MIME_TYPE_VIDEO = 8

interface MimeType {
    val extension: String

    val type: Int
}

val <P : Path> P.mimeType
    get() = DefaultMimeTypeDescriptor.describe(this)
