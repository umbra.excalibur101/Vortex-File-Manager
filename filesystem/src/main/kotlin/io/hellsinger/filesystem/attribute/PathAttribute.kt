package io.hellsinger.filesystem.attribute

import io.hellsinger.filesystem.attribute.time.PathTime

interface PathAttribute {
    val id: Long

    val type: PathType

    val size: Long

    val creationTime: PathTime

    val lastAccessTime: PathTime

    val lastModifiedTime: PathTime

    val permission: String
}
