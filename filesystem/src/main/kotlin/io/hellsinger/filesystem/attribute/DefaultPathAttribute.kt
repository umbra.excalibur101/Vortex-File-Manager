package io.hellsinger.filesystem.attribute

import io.hellsinger.filesystem.attribute.time.PathTime

data class DefaultPathAttribute(
    override val id: Long = EmptyPathAttribute.id,
    override val type: PathType = EmptyPathAttribute.type,
    override val size: Long = EmptyPathAttribute.size,
    override val creationTime: PathTime = EmptyPathAttribute.creationTime,
    override val lastAccessTime: PathTime = EmptyPathAttribute.lastAccessTime,
    override val lastModifiedTime: PathTime = EmptyPathAttribute.lastModifiedTime,
    override val permission: String = EmptyPathAttribute.permission,
) : PathAttribute
