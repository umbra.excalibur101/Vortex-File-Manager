package io.hellsinger.filesystem.attribute

import io.hellsinger.filesystem.attribute.time.PathTime

abstract class PathAttributeWrapper(
    creationTimeNanos: Long,
    creationTimeSeconds: Long,
    lastAccessTimeNanos: Long,
    lastAccessTimeSeconds: Long,
    lastModifiedTimeNanos: Long,
    lastModifiedTimeSeconds: Long,
) : PathAttribute {
    override val creationTime: PathTime =
        PathTime(
            seconds = creationTimeSeconds,
            nanos = creationTimeNanos,
        )
    override val lastAccessTime: PathTime =
        PathTime(
            seconds = lastAccessTimeSeconds,
            nanos = lastAccessTimeNanos,
        )
    override val lastModifiedTime: PathTime =
        PathTime(
            seconds = lastModifiedTimeSeconds,
            nanos = lastModifiedTimeNanos,
        )
}
