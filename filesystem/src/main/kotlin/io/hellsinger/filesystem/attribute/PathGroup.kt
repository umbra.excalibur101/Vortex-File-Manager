package io.hellsinger.filesystem.attribute

interface PathGroup {
    val id: Int
    val name: ByteArray
}
