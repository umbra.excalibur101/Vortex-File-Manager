package io.hellsinger.filesystem.attribute.mimetype

import io.hellsinger.filesystem.path.Path

object DefaultMimeTypeDescriptor : MimeTypeDescriptor {
    override fun describe(path: Path): MimeType {
        val name = path.getNameAt().toString()

        val builder = { ext: String, type: Int ->
            object : MimeType {
                override val extension: String = ext
                override val type: Int = type
            }
        }

        if (name.length <= 1) return builder("", MIME_TYPE_EMPTY)
        val ext = name.substringAfterLast(".")

        if (ext in videoMimeTypes) return builder(ext, MIME_TYPE_VIDEO)
        if (ext in imageMimeTypes) return builder(ext, MIME_TYPE_IMAGE)
        if (ext in applicationMimeTypes) return builder(ext, MIME_TYPE_APPLICATION)
        if (ext in audioMimeTypes) return builder(ext, MIME_TYPE_AUDIO)
        if (ext in textMimeType) return builder(ext, MIME_TYPE_TEXT)

        return builder("", MIME_TYPE_EMPTY)
    }
}
