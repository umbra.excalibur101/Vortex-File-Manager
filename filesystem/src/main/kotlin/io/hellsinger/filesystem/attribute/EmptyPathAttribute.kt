package io.hellsinger.filesystem.attribute

import io.hellsinger.filesystem.attribute.time.PathTime

object EmptyPathAttribute : PathAttribute {
    override val id: Long = -1
    override val type: PathType = PathType.Unknown
    override val size: Long = -1
    override val creationTime: PathTime = PathTime(-1, -1)
    override val lastAccessTime: PathTime = PathTime(-1, -1)
    override val lastModifiedTime: PathTime = PathTime(-1, -1)
    override val permission: String = ""
}
