package io.hellsinger.filesystem.file

import io.hellsinger.filesystem.path.Path
import io.hellsinger.filesystem.path.PathOwner
import io.hellsinger.filesystem.size.Size
import io.hellsinger.filesystem.size.SizeOwner

interface File<P : Path> :
    PathOwner<P>,
    SizeOwner {
    override val path: P

    val name: String
        get() = path.getNameAt().toString()

    override val size: Size
}
