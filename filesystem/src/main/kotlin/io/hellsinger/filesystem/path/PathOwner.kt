package io.hellsinger.filesystem.path

interface PathOwner<P : Path> {
    val path: P
}
