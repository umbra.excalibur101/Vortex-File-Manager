package io.hellsinger.filesystem.path

import java.io.File

interface Path :
    Iterable<Path>,
    Comparable<Path> {
    infix operator fun plus(path: Path): Path = resolve(path)

    infix operator fun get(index: Int): Path = getNameAt(index)

    val nameCount: Int

    val isAbsolute: Boolean

    val isEmpty: Boolean

    val isHidden: Boolean

    val parent: Path?

    val length: Int

    val bytes: ByteArray

    val separator: Byte

    val scheme: Int

    fun getParentAt(index: Int = nameCount - 1): Path?

    fun getNameAt(index: Int = nameCount - 1): Path

    infix fun relativize(other: Path): Path

    infix fun resolve(other: Path): Path

    infix fun resolve(other: ByteArray): Path

    fun subPath(
        from: Int,
        to: Int = length,
    ): Path

    infix fun startsWith(other: Path): Boolean

    infix fun endsWith(other: Path): Boolean

    override fun equals(other: Any?): Boolean

    override fun hashCode(): Int

    override fun toString(): String
}

@Suppress("FunctionName")
fun EmptyPath(): Path =
    object : PathWrapper(byteArrayOf()) {
        override fun onCreatePath(path: ByteArray): Path = EmptyPath()

        override val isHidden: Boolean
            get() = false

        override val isAbsolute: Boolean
            get() = false

        override val separator: Byte
            get() = 0

        override val scheme: Int
            get() = -1
    }

fun Path.toFile(): File = File(toString())
