package io.hellsinger.filesystem.path

interface ObservablePath<P : Path> : PathOwner<P> {
    val event: Int
    override val path: P

    operator fun component1() = event

    operator fun component2() = path
}

fun <P : Path> ObservablePath(
    event: Int,
    path: P,
) = object : ObservablePath<P> {
    override val event: Int = event
    override val path: P = path
}
