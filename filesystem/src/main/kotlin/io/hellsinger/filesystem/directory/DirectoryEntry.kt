package io.hellsinger.filesystem.directory

import io.hellsinger.filesystem.path.Path
import io.hellsinger.filesystem.path.PathOwner

interface DirectoryEntry<P : Path> : PathOwner<P> {
    override val path: P
}
