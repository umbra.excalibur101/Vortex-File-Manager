package io.hellsinger.filesystem.directory

import io.hellsinger.filesystem.path.Path

@Target(AnnotationTarget.TYPE, AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.SOURCE)
annotation class WalkType

interface DirectoryWalker {
    companion object {
        @WalkType
        const val STOP = 0

        @WalkType
        const val SKIP = 1

        @WalkType
        const val CONTINUE = 2
    }

    suspend fun onPreEnterDirectory(path: Path): Int

    suspend fun onPostEnterDirectory(path: Path)

    suspend fun onEntry(path: Path): @WalkType Int
}
