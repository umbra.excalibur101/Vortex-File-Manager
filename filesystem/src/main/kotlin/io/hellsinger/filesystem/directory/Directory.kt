package io.hellsinger.filesystem.directory

import io.hellsinger.filesystem.path.Path
import io.hellsinger.filesystem.path.PathOwner
import kotlinx.coroutines.flow.Flow

interface Directory<P : Path> : PathOwner<P> {
    override val path: P

    fun walk(walker: DirectoryWalker): Flow<DirectoryEntry<P>>
}
