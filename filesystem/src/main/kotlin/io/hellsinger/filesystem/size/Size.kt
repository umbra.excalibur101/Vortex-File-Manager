package io.hellsinger.filesystem.size

interface Size :
    Comparable<Size>,
    Iterable<Int> {
    companion object {
        const val NoType = -1
        const val Bit = 0
        const val KiB = 1
        const val MiB = 2
        const val GiB = 3
        const val TiB = 4
        const val PiB = 5
        const val EiB = 6
        const val ZiB = 7
        const val YiB = 8
    }

    val original: Long

    val type: Int

    val name: String

    val delimiter: Int

    fun format(pattern: String?): Size

    operator fun plus(other: Size): Size

    operator fun minus(other: Size): Size

    override fun equals(other: Any?): Boolean

    override fun toString(): String

    override fun iterator(): Iterator<Int> =
        iterator {
            yield(NoType)
            yield(Bit)
            yield(KiB)
            yield(MiB)
            yield(GiB)
            yield(TiB)
            yield(PiB)
            yield(EiB)
            yield(ZiB)
            yield(YiB)
        }
}
