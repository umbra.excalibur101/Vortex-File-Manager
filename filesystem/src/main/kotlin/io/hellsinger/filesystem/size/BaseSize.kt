package io.hellsinger.filesystem.size

fun BaseSize(
    original: Long,
    delimiter: Int,
    titles: Array<String>,
): SizeWrapper =
    object : SizeWrapper(original) {
        override fun onCreateTypeTitle(type: Int): String = titles[type]

        override fun onCreateSize(original: Long): Size = BaseSize(original, delimiter, titles)

        override val delimiter: Int = delimiter
    }

fun BaseSize(
    original: Long,
    delimiter: Int,
    titles: Map<Int, String>,
): SizeWrapper =
    object : SizeWrapper(original) {
        override fun onCreateTypeTitle(type: Int): String = titles[type]!!

        override fun onCreateSize(original: Long): Size = BaseSize(original, delimiter, titles)

        override val delimiter: Int = delimiter
    }
