package io.hellsinger.filesystem.size

object SizeUnit {
    const val KiB = 1024L
    const val MiB: Long = KiB * KiB
    const val GiB: Long = MiB * MiB
    const val TiB: Long = GiB * GiB

    fun create(
        count: Int,
        bytes: Int,
    ): Int = count * bytes

    fun create(
        count: Long,
        bytes: Long,
    ): Long = count * bytes
}

inline val Int.KiB
    get() = times(SizeUnit.KiB)

inline val Int.MiB
    get() = times(SizeUnit.MiB)

inline val Int.GiB
    get() = times(SizeUnit.GiB)

inline val Int.TiB
    get() = times(SizeUnit.TiB)
