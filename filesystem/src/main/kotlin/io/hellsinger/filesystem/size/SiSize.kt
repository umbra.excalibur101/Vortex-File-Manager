package io.hellsinger.filesystem.size

import io.hellsinger.filesystem.size.Size.Companion.Bit
import io.hellsinger.filesystem.size.Size.Companion.EiB
import io.hellsinger.filesystem.size.Size.Companion.GiB
import io.hellsinger.filesystem.size.Size.Companion.KiB
import io.hellsinger.filesystem.size.Size.Companion.MiB
import io.hellsinger.filesystem.size.Size.Companion.PiB
import io.hellsinger.filesystem.size.Size.Companion.TiB
import io.hellsinger.filesystem.size.Size.Companion.YiB
import io.hellsinger.filesystem.size.Size.Companion.ZiB

@Suppress("FunctionName")
fun SiSize(original: Long): Size =
    object : SizeWrapper(original) {
        override fun onCreateSize(original: Long): Size = SiSize(original)

        override val delimiter: Int = 1000

        override fun onCreateTypeTitle(type: Int) =
            when (type) {
                Bit -> "B"
                KiB -> "KB"
                MiB -> "MB"
                GiB -> "GB"
                TiB -> "TB"
                PiB -> "PB"
                EiB -> "EB"
                ZiB -> "ZB"
                YiB -> "YB"
                else -> throw UnsupportedOperationException("Unsupported type of size")
            }
    }

fun Int.asSiSize(bytes: Long) = SiSize(original = times(bytes))
