package io.hellsinger.filesystem.size

interface SizeOwner {
    val size: Size
}
