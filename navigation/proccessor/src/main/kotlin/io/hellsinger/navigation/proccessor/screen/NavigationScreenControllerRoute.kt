package io.hellsinger.navigation.proccessor.screen

@Target(AnnotationTarget.CLASS)
annotation class NavigationScreenControllerRoute(
    val name: String,
)
