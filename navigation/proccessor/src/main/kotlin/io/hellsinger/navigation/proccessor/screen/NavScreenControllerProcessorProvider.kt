package io.hellsinger.navigation.proccessor.screen

import com.google.devtools.ksp.processing.SymbolProcessor
import com.google.devtools.ksp.processing.SymbolProcessorEnvironment
import com.google.devtools.ksp.processing.SymbolProcessorProvider

class NavScreenControllerProcessorProvider : SymbolProcessorProvider {
    override fun create(environment: SymbolProcessorEnvironment): SymbolProcessor =
        NavScreenControllerProcessor(
            generator = environment.codeGenerator,
            logger = environment.logger,
        )
}
