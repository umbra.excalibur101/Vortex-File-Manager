plugins {
    alias(libs.plugins.kotlin.jvm)
}

dependencies {
    implementation(libs.google.ksp.api)
}
