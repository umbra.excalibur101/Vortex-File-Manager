package io.hellsinger.navigation.data

class DefaultNavDataStack<D> : NavDataStack<D> {
    private val stack = ArrayList<D>(5)
    override var current: Int = -1
        private set

    override val size: Int
        get() = stack.size

    override fun get(index: Int): D = stack[index]

    override fun push(dest: D) {
        stack.add(dest)
    }

    fun next() {
        current++
    }

    fun previous() {
        current--
    }

    override fun pop(): D = stack.removeAt(current--)

    fun clear() {
        stack.clear()
        current = -1
    }
}
