package io.hellsinger.navigation.data

class DefaultNavDataController<D> : NavDataController<D> {
    private val _stack = DefaultNavDataStack<D>()
    override val stack: NavDataStack<D> = _stack

    val currentDest: D
        get() = _stack[stack.current]

    override fun navigate(dest: D) {
        _stack.push(dest)
    }

    fun navigateBack() = _stack.pop()

    fun init(dest: D) {
        _stack.clear()
        _stack.push(dest)
    }
}
