package io.hellsinger.navigation.data

import io.hellsinger.navigation.NavController

/**
 * D e.g. Data
 * **/
interface NavDataController<D> : NavController {
    val stack: NavDataStack<D>

    fun navigate(dest: D)
}
