package io.hellsinger.navigation.data

import io.hellsinger.navigation.NavStack

interface NavDataStack<D> : NavStack<D> {
    override val size: Int

    override val current: Int

    override operator fun get(index: Int): D
}
