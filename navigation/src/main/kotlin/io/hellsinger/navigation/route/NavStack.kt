package io.hellsinger.navigation.route

import io.hellsinger.navigation.screen.ScreenController

interface NavStack {
    val size: Int

    val current: Int

    fun push(value: ScreenController<*>)

    fun pop(): ScreenController<*>

    operator fun get(index: Int): ScreenController<*>
}
