package io.hellsinger.navigation.route

fun interface RouteMapper<T> {
    fun onMapRoute(route: String): T
}
