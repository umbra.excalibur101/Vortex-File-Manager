package io.hellsinger.navigation.route

import android.os.Bundle
import io.hellsinger.navigation.screen.ScreenController

interface AndroidRouter : Router {
    fun save(buffer: Bundle)

    fun restore(buffer: Bundle)

    fun <A> navigate(
        screen: ScreenController<A>,
        args: A? = null,
    )

    fun <T : ScreenController<*>> popTo(controller: Class<T>)
}

inline fun <reified T : ScreenController<*>> AndroidRouter.popTo() = popTo(T::class.java)
