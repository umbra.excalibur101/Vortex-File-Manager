package io.hellsinger.navigation.route

fun interface ArgsAdapter<I, O> {
    fun create(input: I): O
}

inline fun <I, O> argsAdapter(crossinline block: (I) -> O): ArgsAdapter<I, O> =
    ArgsAdapter { input ->
        block(input)
    }
