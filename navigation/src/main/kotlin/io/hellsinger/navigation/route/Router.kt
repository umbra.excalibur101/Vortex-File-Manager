package io.hellsinger.navigation.route

interface Router {
    fun <A> navigate(
        route: String,
        args: A? = null,
    )

    fun navigateBack()

    fun popTo(route: String)

    fun init(route: String)
}
