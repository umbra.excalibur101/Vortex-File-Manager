package io.hellsinger.navigation.route

import io.hellsinger.navigation.screen.ScreenController

class DefaultNavStack : NavStack {
    private val stack = mutableListOf<ScreenController<*>>()

    override val size: Int
        get() = stack.size

    override var current: Int = size - 1

    override fun get(index: Int): ScreenController<*> = stack[index]

    override fun push(value: ScreenController<*>) = stack.add(++current, value)

    override fun pop(): ScreenController<*> = stack.removeAt(current--)
}
