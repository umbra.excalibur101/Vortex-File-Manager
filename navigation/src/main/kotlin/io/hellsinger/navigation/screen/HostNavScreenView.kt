package io.hellsinger.navigation.screen

import android.content.Context
import android.util.AttributeSet
import android.view.WindowInsets
import android.widget.FrameLayout

class HostNavScreenView : FrameLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr,
    )

    override fun onApplyWindowInsets(insets: WindowInsets): WindowInsets = insets

    override fun dispatchApplyWindowInsets(insets: WindowInsets): WindowInsets {
        val dispatched = onApplyWindowInsets(insets)
        if (!insets.isConsumed) {
            for (i in 0 until childCount) getChildAt(i).dispatchApplyWindowInsets(dispatched)
        }

        return insets
    }
}
