package io.hellsinger.navigation.screen

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class ListScreenController<Args> : ScreenController<Args>() {
    protected abstract val adapter: RecyclerView.Adapter<*>
    protected var recycler: RecyclerView? = null

    override fun onCreateViewImpl(context: Context): View {
        recycler = RecyclerView(context)
        onApplyRecyclerView(recycler!!)
        return recycler!!
    }

    protected open fun onApplyRecyclerView(recycler: RecyclerView) {
        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(recycler.context)
    }

    override fun onDestroyImpl() {
        recycler?.removeAllViews()
        recycler = null
    }
}
