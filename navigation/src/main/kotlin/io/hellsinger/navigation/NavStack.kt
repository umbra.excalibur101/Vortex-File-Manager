package io.hellsinger.navigation

interface NavStack<T> {
    val size: Int

    val current: Int

    fun push(value: T)

    fun pop(): T

    operator fun get(index: Int): T
}
