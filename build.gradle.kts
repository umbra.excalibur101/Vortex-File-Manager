import org.jetbrains.kotlin.gradle.dsl.JvmTarget

plugins {
    alias(libs.plugins.android.application) apply false
    alias(libs.plugins.android.library) apply false
//    alias(libs.plugins.google.ksp) apply false
    alias(libs.plugins.kotlin.android) apply false
    alias(libs.plugins.kotlin.parcelize) apply false
    alias(libs.plugins.kotlin.jvm) apply false
    alias(libs.plugins.android.test) apply false
    alias(libs.plugins.androidx.baseline.profile) apply false
    alias(libs.plugins.ktlint) apply false
}

tasks.create<Delete>(
    name = "clean",
    configuration = {
        delete(rootProject.layout.buildDirectory)
    },
)

subprojects {
    val args =
        listOf(
            "-opt-in=kotlin.RequiresOptIn",
            "-Xcontext-receivers",
        )

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        compilerOptions {
            jvmTarget = JvmTarget.JVM_21
            freeCompilerArgs = args
        }
    }
}
