# Vortex File Manager (Vortex)

This file created for developers or contributors who wants to improve existed or add new features,
plugins, etc.

## Feature

If you want to add new screen (feature), you must implement `ScreenController` class.

This class simply represents 'screen', you can control your data and views with it, but its
recommend to use `ViewModel` class,

## Conceptions

### Transactions

Instead of simple file operation (e.g. delete, create, move), Vortex represents transactions.
It's same as file operation except information and confirmation by user. That's about path, size,
etc. transitions or modifications (for example you can check StorageTransactionScreenController).

It gives user more information and even more understanding what we're going to do with file system

#### Type

Currently, Vortex represents 2 types of transaction: Modification and Transition

__Modification__ - represents transitions such as delete, create, etc. In other context:
it's a simple array of path-sources without additional path-destinations.

```kotlin

// Lets say you have 10 sources, we need to delete them

fun delete(sources: List<String>) {
    for (source in sources) println("Delete $source...")
}

fun main() {
    val sources = buildList {
        repeat(10) { i ->
            add("source $i")
        }
    }
    // Such operations we call it Modification
    delete(sources)
}
```

__Transition__ - represents transitions such as move, copy, swap, etc. In other context:
it's a simple array of path-sources with additional path-destinations.

```kotlin

// Lets say you have 10 sources and 1 destination, we need to move them

fun move(sources: List<String>, dest: String) {
    for (source in sources) println("Moving $source to $dest...")
}

fun main() {
    val sources = buildList {
        repeat(10) { i ->
            add("source $i")
        }
    }
    val dest = "dest"
    // Such operations we call it Transition
    move(sources, dest)
}
```