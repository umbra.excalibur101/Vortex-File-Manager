package io.hellsinger.vortex.service.notification

import android.app.Notification
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import io.hellsinger.theme.vortex.VortexSettings

class ServiceNotifier(
    private val context: Context,
) : TransactionNotifier {
    private val manager = NotificationManagerCompat.from(context)

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManagerCompat.IMPORTANCE_LOW

            val operationChannel =
                NotificationChannelCompat
                    .Builder(OPERATION_CHANNEL_ID, importance)
                    .setName("Vortex Transaction channel")
                    .setDescription(VortexSettings.texts { key_storage_service_operation_channel_description })
                    .build()

            val mainChannel =
                NotificationChannelCompat
                    .Builder(MAIN_CHANNEL_ID, importance)
                    .setName("Vortex Service channel")
                    .setDescription(VortexSettings.texts { key_storage_service_vortex_channel_description })
                    .build()

            manager.createNotificationChannel(operationChannel)
            manager.createNotificationChannel(mainChannel)
        }
    }

    override fun post(
        id: Int,
        notification: Notification,
    ) {
        manager.notify(id, notification)
    }

    override fun cancel(id: Int) {
        manager.cancel(id)
    }

    override fun getTransactionChannelBuilder(): NotificationCompat.Builder = NotificationCompat.Builder(context, OPERATION_CHANNEL_ID)

    companion object {
        private const val MAIN_CHANNEL_ID = "vortex_service_main_channel"
        private const val OPERATION_CHANNEL_ID = "vortex_service_operation_channel"
    }
}
