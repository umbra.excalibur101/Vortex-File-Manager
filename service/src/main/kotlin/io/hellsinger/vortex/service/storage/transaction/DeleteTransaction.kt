package io.hellsinger.vortex.service.storage.transaction

import androidx.core.graphics.drawable.toBitmap
import io.hellsinger.filesystem.linux.directory.asLinuxDirectory
import io.hellsinger.filesystem.linux.error.ErrnoException
import io.hellsinger.filesystem.linux.file.asLinuxFile
import io.hellsinger.filesystem.linux.operation.delete
import io.hellsinger.theme.vortex.formatter
import io.hellsinger.vortex.data.model.OperationState
import io.hellsinger.vortex.data.model.PathItem
import io.hellsinger.vortex.service.model.Source
import io.hellsinger.vortex.service.model.isDirectory
import io.hellsinger.vortex.ui.icon.Icons

class DeleteTransaction(
    private val sources: List<Source>,
) : StorageTransaction() {
    private var progress = 0

    override suspend fun perform(): Result {
        for (i in sources.indices) {
            val source = sources[i]
            try {
                if (source.isDirectory) deleteDirectory(source) else deleteFile(source)
            } catch (errno: ErrnoException) {
                // TODO: Implement error interception
            }
        }

        updateNotification {
            setContentTitle("Delete operation")
            setContentTitle("$progress of ${sources.size} successfully deleted")
            setProgress(0, 0, false)
            setSmallIcon(io.hellsinger.vortex.icon.R.drawable.ic_folder_24)
            setLargeIcon(Icons.Rounded.Delete?.toBitmap())
        }

        closeNotificationAfter(DEFAULT_NOTIFICATION_TIMEOUT)

        return Result.Success
    }

    private suspend inline fun deleteDirectory(directory: Source) {
        directory.path.asLinuxDirectory().delete()
        ++progress
        updateOperationState {
            OperationState.Modification(
                id = id,
                type = TransactionTypes.DELETE,
                progress = progress.toLong(),
                max = sources.size.toLong(),
                source = PathItem(directory),
            )
        }
        updateNotification {
            setContentTitle("Delete transaction")
            setSmallIcon(io.hellsinger.vortex.icon.R.drawable.ic_folder_24)
            setLargeIcon(Icons.Rounded.Delete?.toBitmap())
            setContentText(
                formatter(directory.path.getNameAt().toString()) {
                    key_storage_service_item_deleted_message
                },
            )
            setProgress(sources.size, progress, false)
        }
    }

    private suspend inline fun deleteFile(file: Source) {
        file.path.asLinuxFile().delete()
        ++progress
        updateOperationState {
            OperationState.Modification(
                id = id,
                type = TransactionTypes.DELETE,
                progress = progress.toLong(),
                max = sources.size.toLong(),
                source = PathItem(file),
            )
        }
        updateNotification {
            setContentTitle("Delete operation")
            setSmallIcon(io.hellsinger.vortex.icon.R.drawable.ic_folder_24)
            setLargeIcon(Icons.Rounded.Delete?.toBitmap())
            setContentText(
                formatter(file.path.getNameAt().toString()) {
                    key_storage_service_item_deleted_message
                },
            )
            setProgress(sources.size, progress, false)
        }
    }
}
