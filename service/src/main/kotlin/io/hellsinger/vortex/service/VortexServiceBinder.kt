package io.hellsinger.vortex.service

import android.os.Binder
import io.hellsinger.vortex.service.di.ServiceContainer
import io.hellsinger.vortex.service.storage.TransactionController

class VortexServiceBinder(
    internal val container: ServiceContainer,
) : Binder() {
    val storage =
        TransactionController(
            notifier = container.notifier,
            dispatchers = container.dispatchers,
        )
}
