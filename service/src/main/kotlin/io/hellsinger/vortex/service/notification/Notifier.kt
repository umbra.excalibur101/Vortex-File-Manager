package io.hellsinger.vortex.service.notification

import android.app.Notification

interface Notifier {
    fun post(
        id: Int,
        notification: Notification,
    )

    fun cancel(id: Int)
}
