package io.hellsinger.vortex.service.storage.transaction

import io.hellsinger.filesystem.linux.error.ErrnoException
import io.hellsinger.filesystem.linux.operation.changeMode
import io.hellsinger.vortex.service.model.Source

class PermissionTransaction(
    private val source: Source,
    private val mode: Int,
) : StorageTransaction() {
    override suspend fun perform(): Result {
        try {
            source.path.changeMode(mode)
        } catch (errno: ErrnoException) {
        }

        return Result.Success
    }

    companion object {
        const val MODE_PARAM_KEY = "mode"
    }
}
