package io.hellsinger.vortex.service.di

import io.hellsinger.vortex.Dispatchers
import io.hellsinger.vortex.data.repository.StorageOperationStateProducer
import io.hellsinger.vortex.service.notification.ServiceNotifier

interface ServiceDependencies {
    val dispatchers: Dispatchers
    val notifier: ServiceNotifier
    val producer: StorageOperationStateProducer
}
