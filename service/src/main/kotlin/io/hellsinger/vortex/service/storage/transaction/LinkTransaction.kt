package io.hellsinger.vortex.service.storage.transaction

import io.hellsinger.filesystem.linux.error.ErrnoException
import io.hellsinger.filesystem.linux.operation.createSymbolicLink
import io.hellsinger.vortex.service.model.Dest
import io.hellsinger.vortex.service.model.Source

class LinkTransaction(
    private val dest: Dest,
    private val source: Source,
    private val isHard: Boolean,
) : StorageTransaction() {
    override suspend fun perform(): Result {
        try {
            if (isHard) {
                // TODO: Add support for hard links
//                source.link()
            } else {
                source.path.createSymbolicLink(dest.path)
            }
        } catch (exception: ErrnoException) {
        }

        closeNotificationAfter(DEFAULT_NOTIFICATION_TIMEOUT)

        return Result.Success
    }

    companion object {
        const val HARD_PARAM_KEY = "isHard"
        const val HARD_PARAM_DEFAULT_VALUE = 0
        const val HARD_PARAM_TYPE_HARD = 1
    }
}
