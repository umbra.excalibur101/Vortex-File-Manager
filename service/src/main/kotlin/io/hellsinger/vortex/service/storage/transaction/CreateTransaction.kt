package io.hellsinger.vortex.service.storage.transaction

import androidx.core.graphics.drawable.toBitmap
import io.hellsinger.filesystem.linux.directory.asLinuxDirectory
import io.hellsinger.filesystem.linux.file.asLinuxFile
import io.hellsinger.filesystem.linux.operation.create
import io.hellsinger.theme.vortex.formatter
import io.hellsinger.vortex.service.model.Dest
import io.hellsinger.vortex.ui.icon.Icons

class CreateTransaction(
    private val dest: Dest,
    private val mode: Int = 777,
    private val isDirectory: Boolean,
) : StorageTransaction() {
    override suspend fun perform(): Result {
        try {
            if (isDirectory) {
                dest.path.asLinuxDirectory().create(mode = mode)
            } else {
                dest.path.asLinuxFile().create(mode = mode)
            }
            updateNotification {
                setContentTitle("Create transaction")
                setSmallIcon(io.hellsinger.vortex.icon.R.drawable.ic_folder_24)
                setLargeIcon(Icons.Rounded.FolderAdd?.toBitmap())
                setContentText(
                    formatter(
                        dest.path.getNameAt(),
                        dest.path.parent?.getNameAt(),
                    ) { key_storage_service_item_created_message },
                )
            }

            closeNotificationAfter(1000L)

            return Result.Success
        } catch (exception: Throwable) {
            return Result.Error(exception)
        }
    }

    companion object {
        const val MODE_PARAM_KEY = "mode"
        const val MODE_PARAM_DEFAULT_VALUE = 777
        const val DIRECTORY_PARAM_KEY = "isDirectory"
        const val DIRECTORY_PARAM_DEFAULT_VALUE = 0
        const val DIRECTOR_PARAM_TYPE_DIRECTORY = 1
    }
}
