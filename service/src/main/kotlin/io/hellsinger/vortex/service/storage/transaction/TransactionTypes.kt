package io.hellsinger.vortex.service.storage.transaction

object TransactionTypes {
    const val DELETE = 1 shl 0
    const val CREATE = 1 shl 1
    const val RENAME = 1 shl 2
    const val COPY = 1 shl 3
    const val MOVE = 1 shl 4
}
