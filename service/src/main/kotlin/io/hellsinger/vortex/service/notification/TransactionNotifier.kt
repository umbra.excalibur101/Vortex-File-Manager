package io.hellsinger.vortex.service.notification

import androidx.core.app.NotificationCompat

interface TransactionNotifier : Notifier {
    fun getTransactionChannelBuilder(): NotificationCompat.Builder
}
