package io.hellsinger.vortex.service

import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder
import io.hellsinger.vortex.service.di.ServiceContainer

class VortexService : Service() {
    private val binder by lazy(LazyThreadSafetyMode.NONE) {
        VortexServiceBinder(ServiceContainer(context = this))
    }

    override fun onBind(intent: Intent?): IBinder? {
        if (intent == null) return null

        return binder
    }

    override fun onStartCommand(
        intent: Intent?,
        flags: Int,
        startId: Int,
    ): Int = START_STICKY

    override fun onUnbind(intent: Intent?): Boolean = super.onUnbind(intent)

    override fun onDestroy() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) stopForeground(STOP_FOREGROUND_REMOVE)
        super.onDestroy()
    }
}
