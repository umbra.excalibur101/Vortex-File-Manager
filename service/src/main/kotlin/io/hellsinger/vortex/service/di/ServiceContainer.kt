package io.hellsinger.vortex.service.di

import android.content.Context
import io.hellsinger.vortex.Dispatchers
import io.hellsinger.vortex.data.repository.StorageOperationStateProducer
import io.hellsinger.vortex.navigation.findDependencies
import io.hellsinger.vortex.service.notification.ServiceNotifier

class ServiceContainer(
    private val context: Context,
) : ServiceDependencies {
    val parent = context.findDependencies<ServiceDependencies>()
    override val dispatchers: Dispatchers = parent.dispatchers
    override val notifier: ServiceNotifier = parent.notifier
    override val producer: StorageOperationStateProducer = parent.producer
}
