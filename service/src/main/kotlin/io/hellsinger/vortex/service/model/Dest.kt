package io.hellsinger.vortex.service.model

import android.os.Parcelable
import io.hellsinger.filesystem.path.Path
import io.hellsinger.filesystem.path.PathOwner
import io.hellsinger.vortex.data.model.PathItem
import io.hellsinger.vortex.data.model.PathParceler
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith

interface Dest : Parcelable, PathOwner<Path> {
    override val path: Path
}

@Parcelize
private data class DefaultDestImpl(
    override val path: @WriteWith<PathParceler> Path,
) : Dest

fun Dest(path: Path): Dest = DefaultDestImpl(path)

fun Dest(owner: PathOwner<*>): Dest = DefaultDestImpl(owner.path)

fun PathItem.asDest(): Dest = Dest(path)
