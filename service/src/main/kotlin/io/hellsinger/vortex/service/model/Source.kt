package io.hellsinger.vortex.service.model

import android.os.Parcelable
import io.hellsinger.filesystem.attribute.PathAttribute
import io.hellsinger.filesystem.attribute.PathType
import io.hellsinger.filesystem.path.Path
import io.hellsinger.filesystem.path.PathOwner
import io.hellsinger.vortex.data.model.PathItem
import io.hellsinger.vortex.data.model.PathParceler
import io.hellsinger.vortex.data.model.PathTypeParceler
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith

interface Source :
    Parcelable,
    PathOwner<Path> {
    override val path: Path

    val type: PathType
}

inline val Source.isDirectory
    get() = type == PathType.Directory
inline val Source.isFile
    get() = type == PathType.File
inline val Source.isLink
    get() = type == PathType.Link
inline val Source.isPipe
    get() = type == PathType.Pipe

@Parcelize
private data class DefaultSourceImpl(
    override val path: @WriteWith<PathParceler> Path,
    override val type: @WriteWith<PathTypeParceler> PathType,
) : Source

fun Source(
    path: Path,
    attributes: PathAttribute,
): Source =
    when (attributes.type) {
        PathType.Directory -> directory(path)
        PathType.Link -> link(path)
        PathType.Pipe -> pipe(path)
        else -> file(path)
    }

fun PathItem.asSource(): Source = Source(path, attrs)

fun directory(path: Path): Source = DefaultSourceImpl(path, PathType.Directory)

fun file(path: Path): Source = DefaultSourceImpl(path, PathType.File)

fun link(path: Path): Source = DefaultSourceImpl(path, PathType.Link)

fun pipe(path: Path): Source = DefaultSourceImpl(path, PathType.Pipe)
