package io.hellsinger.vortex.service.storage.transaction

import io.hellsinger.filesystem.linux.error.ErrnoException
import io.hellsinger.filesystem.linux.operation.rename
import io.hellsinger.vortex.service.model.Dest
import io.hellsinger.vortex.service.model.Source

class RenameTransaction(
    private val source: Source,
    private val dest: Dest,
) : StorageTransaction() {
    override suspend fun perform(): Result {
        try {
            source.path.rename(dest.path)
        } catch (error: ErrnoException) {
        }

        return Result.Success
    }
}
