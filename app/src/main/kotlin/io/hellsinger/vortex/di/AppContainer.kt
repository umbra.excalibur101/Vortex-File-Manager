package io.hellsinger.vortex.di

import android.content.Context
import io.hellsinger.filesystem.linux.operation.LinuxPathObserver
import io.hellsinger.vortex.DefaultDispatchers
import io.hellsinger.vortex.Dispatchers
import io.hellsinger.vortex.data.repository.AndroidPathObservation
import io.hellsinger.vortex.data.repository.AndroidPermissions
import io.hellsinger.vortex.data.repository.AndroidRestrictedStorageRepository
import io.hellsinger.vortex.data.repository.AndroidStorageOperationStateProducer
import io.hellsinger.vortex.data.repository.AndroidStorageRepository
import io.hellsinger.vortex.data.repository.StorageOperationStateConsumer
import io.hellsinger.vortex.data.repository.StorageOperationStateProducer
import io.hellsinger.vortex.domain.repository.PathObservation
import io.hellsinger.vortex.domain.repository.StorageRepository
import io.hellsinger.vortex.service.di.ServiceDependencies
import io.hellsinger.vortex.service.notification.ServiceNotifier
import io.hellsinger.vortex.util.DefaultPathItemDescriptor
import io.hellsinger.vortex.util.PathItemDescriptor
import io.hellsinger.vortex.util.SpeedStoragePathSegmentParser
import io.hellsinger.vortex.util.StoragePathSegmentParser

class AppContainer(
    context: Context,
) : StorageListDependencies,
    StorageMediaDependencies,
    StorageItemDetailDependencies,
    StorageFileEditorDependencies,
    StorageCreateItemDependencies,
    StorageTransactionDependencies,
    StorageRenameItemDependencies,
    ServiceDependencies {
    private val observer = LinuxPathObserver()

    override val repository: StorageRepository = AndroidStorageRepository()
    override val parser: StoragePathSegmentParser = SpeedStoragePathSegmentParser()
    override val dispatchers: Dispatchers = DefaultDispatchers()
    override val permissions: AndroidPermissions = AndroidPermissions(context)
    override val restricted: AndroidRestrictedStorageRepository =
        AndroidRestrictedStorageRepository()
    override val notifier: ServiceNotifier by lazy { ServiceNotifier(context) }
    override val observation: PathObservation = AndroidPathObservation(observer)
    override val producer: StorageOperationStateProducer = AndroidStorageOperationStateProducer()
    override val consumer: StorageOperationStateConsumer = producer
    override val descriptor: PathItemDescriptor = DefaultPathItemDescriptor()
}
