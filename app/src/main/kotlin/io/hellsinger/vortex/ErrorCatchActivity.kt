package io.hellsinger.vortex

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.Button
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams
import android.widget.TextView
import androidx.core.content.getSystemService
import io.hellsinger.filesystem.linux.error.ErrnoException
import io.hellsinger.vortex.foundation.bundle.serializable
import io.hellsinger.vortex.foundation.logger.logIt
import io.hellsinger.vortex.ui.dsl.button
import io.hellsinger.vortex.ui.dsl.frame
import io.hellsinger.vortex.ui.dsl.onClick
import io.hellsinger.vortex.ui.dsl.text

class ErrorCatchActivity :
    Activity(),
    View.OnClickListener {
    private var root: FrameLayout? = null
    private var copy: Button? = null
    private var message: TextView? = null
    private val error by lazy(LazyThreadSafetyMode.NONE) {
        intent.extras?.serializable<Throwable>(ERROR_KEY)
    }

    override fun onCreate(inState: Bundle?) {
        super.onCreate(inState)

        root =
            frame(this) {
                fitsSystemWindows = true
                layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)

                message =
                    text {
                        layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
                        isNestedScrollingEnabled = true
                        isVerticalScrollBarEnabled = true
                        text = this@ErrorCatchActivity.error.toString()
                    }

                copy =
                    button {
                        layoutParams =
                            LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                                gravity = Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL
                            }
                        text = "Copy stack trace"

                        onClick(this@ErrorCatchActivity)
                    }
            }

        setContentView(root)
    }

    override fun onClick(v: View) {
        val error = error ?: return
        if (error is ErrnoException) return copyErrno(error)

        copyThrowable(error)
    }

    private fun copyErrno(error: ErrnoException) {
        error.message.logIt(type = Log.ERROR)
        val clipboard = getSystemService<ClipboardManager>()

        val clip = ClipData.newPlainText("vortex_stack_trace", error.message)

        clipboard?.setPrimaryClip(clip)
    }

    private fun copyThrowable(error: Throwable) {
        val trace = error.fillInStackTrace().stackTraceToString()
        trace.logIt(type = Log.ERROR)
        val clipboard = getSystemService<ClipboardManager>()

        val clip = ClipData.newPlainText("vortex_stack_trace", trace)

        clipboard?.setPrimaryClip(clip)
    }

    companion object {
        const val ERROR_KEY = "error"
    }
}
