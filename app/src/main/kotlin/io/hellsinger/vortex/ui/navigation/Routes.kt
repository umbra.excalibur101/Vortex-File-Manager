package io.hellsinger.vortex.ui.navigation

import io.hellsinger.navigation.screen.ScreenController

object Routes {
    object Storage {
        const val LIST = "storage_list_screen"
        const val ITEM_DETAIL = "storage_item_detail_screen"
        const val CREATE = "storage_create_screen"
        const val RENAME = "storage_rename_screen"
        const val FILE_EDITOR = "storage_file_editor_screen"
        const val SCHEDULE = "storage_file_schedule"
    }
}

inline operator fun String.invoke(block: () -> ScreenController<*>): ScreenController<*> = block()
