package io.hellsinger.vortex.ui.screen

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout.LayoutParams
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout.LayoutParams.WRAP_CONTENT
import androidx.recyclerview.widget.LinearLayoutManager
import io.hellsinger.navigation.screen.ScreenController
import io.hellsinger.theme.vortex.VortexSettings
import io.hellsinger.vortex.R
import io.hellsinger.vortex.ui.component.adapter.ItemAdapter
import io.hellsinger.vortex.ui.component.adapter.ItemViewProvider
import io.hellsinger.vortex.ui.component.adapter.Items
import io.hellsinger.vortex.ui.component.adapter.UnsupportedViewTypeException
import io.hellsinger.vortex.ui.component.adapter.drawer
import io.hellsinger.vortex.ui.component.adapter.listener.ItemListener
import io.hellsinger.vortex.ui.component.adapter.title
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.drawable.RoundedCornerDrawable
import io.hellsinger.vortex.ui.component.item.drawer.DrawerItemView
import io.hellsinger.vortex.ui.component.item.title.TitleItemView
import io.hellsinger.vortex.ui.dsl.recycler
import io.hellsinger.vortex.ui.icon.Icons
import io.hellsinger.vortex.ui.screen.settings.SettingsScreenController
import io.hellsinger.vortex.ui.screen.storage.list.StorageListScreenController

class BottomDrawerNavigationScreenController(
    private val context: Context,
) : ScreenController<Unit>(),
    ItemListener {
    private val adapter =
        ItemAdapter(
            listener = this,
            viewProvider =
                ItemViewProvider { parent, viewType ->
                    when (viewType) {
                        Items.TITLE -> {
                            TitleItemView(parent.context).apply {
                                setTitleColor(VortexSettings.colors { key_navigation_drawer_item_title_color })
                            }
                        }

                        Items.DRAWER -> {
                            DrawerItemView(parent.context).apply {
                                setTitleColor(VortexSettings.colors { key_navigation_drawer_item_title_color })
                                setIconColor(VortexSettings.colors { key_navigation_drawer_item_icon_color })
                                setRippleColor(VortexSettings.colors { key_navigation_drawer_item_ripple_color })
                                setBackgroundColor(VortexSettings.colors { key_navigation_drawer_item_surface_color })
                            }
                        }

                        else -> throw UnsupportedViewTypeException(viewType)
                    }
                },
        ) {
            title(VortexSettings.texts { key_navigation_drawer_title })
            drawer(
                R.id.app_bottom_drawer_navigation_file_manager,
                VortexSettings.texts { key_navigation_drawer_item_file_manager },
                Icons.Rounded.Directory,
            )
            drawer(
                R.id.app_bottom_drawer_navigation_settings,
                VortexSettings.texts { key_navigation_drawer_item_settings },
                Icons.Rounded.Settings,
            )
        }

    override val usePopupMode: Boolean
        get() = true

    override fun onPrepareImpl() {
    }

    override fun onDestroyImpl() {
    }

    override fun onItemClick(
        view: View,
        position: Int,
    ) {
        when (view.id) {
            R.id.app_bottom_drawer_navigation_file_manager -> {
                router?.navigate(StorageListScreenController(context = context))
            }

            R.id.app_bottom_drawer_navigation_settings -> {
                router?.navigate(SettingsScreenController(context = context))
            }
        }
    }

    override fun onLongItemClick(
        view: View,
        position: Int,
    ): Boolean = false

    override fun onCreateViewImpl(context: Context): View =
        recycler(context) {
            adapter = this@BottomDrawerNavigationScreenController.adapter
            layoutManager = LinearLayoutManager(context)
            layoutParams =
                LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                    gravity = Gravity.BOTTOM
                }
            background =
                RoundedCornerDrawable(
                    color = VortexSettings.colors { key_navigation_drawer_background_color },
                    topLeft = 16F.dp,
                    topRight = 16F.dp,
                )
        }

    override fun onFocusImpl() {
    }

    override fun onHideImpl() {
    }

    override fun onBackActivatedImpl(): Boolean = false

    override fun onRestoreImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean = false

    override fun onSaveImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean = false
}
