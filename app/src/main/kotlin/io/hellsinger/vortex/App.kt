package io.hellsinger.vortex

import android.app.Application
import android.content.res.Configuration.UI_MODE_NIGHT_MASK
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import io.hellsinger.theme.vortex.VortexSettings
import io.hellsinger.theme.vortex.initialize
import io.hellsinger.vortex.di.AppContainer
import io.hellsinger.vortex.navigation.ComponentProvider
import io.hellsinger.vortex.ui.icon.IconInitializer

class App :
    Application(),
    ComponentProvider<AppContainer> {
    private var container: AppContainer? = null
    private val crashHandler = CrashHandler(this)

    override fun provide(): AppContainer = checkNotNull(container)

    override fun onCreate() {
        super.onCreate()
        container = AppContainer(this)

        val isDarkMode = resources.configuration.uiMode and UI_MODE_NIGHT_MASK == UI_MODE_NIGHT_YES
        VortexSettings.texts.initialize()
        VortexSettings.colors.initialize(isDarkMode)

        IconInitializer.context = this

        Thread.setDefaultUncaughtExceptionHandler(crashHandler)
    }
}
