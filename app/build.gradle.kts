import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties

plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.parcelize)
    alias(libs.plugins.androidx.baseline.profile)
    alias(libs.plugins.ktlint)
}

ktlint {
    android.set(true)
    outputColorName.set("RED")
}

android {
    namespace = "io.hellsinger.vortex"
    compileSdk = AndroidConfigure.targetSdk

    defaultConfig {
        applicationId = AndroidConfigure.applicationId
        minSdk = AndroidConfigure.minSdk
        targetSdk = AndroidConfigure.targetSdk
        versionCode = AndroidConfigure.versionCode
        versionName = AndroidConfigure.versionName
        multiDexEnabled = AndroidConfigure.multiDexEnabled
    }

    buildFeatures {
        buildConfig = true
    }

    signingConfigs {
        create(BuildConfig.Type.Release.Name) {
            val props = gradleLocalProperties(rootDir, providers)
            storeFile = file(props["signing.path"].toString())
            storePassword = props["signing.pathPassword"].toString()
            keyAlias = props["signing.alias"].toString()
            keyPassword = props["signing.aliasPassword"].toString()
        }
    }

    buildTypes {
        getByName(BuildConfig.Type.Release.Name) {
            isMinifyEnabled = BuildConfig.Type.Release.isMinifyEnabled
            isShrinkResources = BuildConfig.Type.Release.isShrinkResources
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro",
            )
            signingConfig = signingConfigs[BuildConfig.Release.Name]
        }
    }

    compileOptions {
        sourceCompatibility = BuildConfig.JDK.VerEnum
        targetCompatibility = BuildConfig.JDK.VerEnum
    }
}

baselineProfile {
    dexLayoutOptimization = true
}

dependencies {
    implementation(libs.androidx.core)
    implementation(libs.androidx.collection)
    implementation(libs.androidx.recycler)
    implementation(libs.androidx.viewpager)

    implementation(libs.kotlinx.coroutines.android)

    implementation(projects.core.ui)
    implementation(projects.core.ui.icon)
    implementation(projects.core.data)
    implementation(projects.core.domain)
    implementation(projects.core.common)
    implementation(projects.core.navigation)

    implementation(projects.filesystem)
    implementation(projects.filesystem.linux)

    implementation(projects.service)

    implementation(projects.navigation)

    implementation(projects.vortexFoundation)

    implementation(projects.theme)
    implementation(projects.theme.vortex)

    // Storage
    implementation(projects.feature.list)
    implementation(projects.feature.create)
    implementation(projects.feature.detail)
    implementation(projects.feature.schedule)
    implementation(projects.feature.editor)
    implementation(projects.feature.transaction)
    implementation(projects.feature.rename)
    implementation(projects.feature.mediaViewer)

    // Settings
    implementation(projects.feature.settings)

    implementation(libs.androidx.profile.installer)
    "baselineProfile"(project(":vortex-baseline-profile"))
}
