package io.hellsinger.viewmodel.android

import android.os.Bundle

interface SavableViewModel {
    fun save(
        buffer: Bundle,
        prefix: String,
    ): Boolean

    fun restore(
        buffer: Bundle,
        prefix: String,
    ): Boolean
}
