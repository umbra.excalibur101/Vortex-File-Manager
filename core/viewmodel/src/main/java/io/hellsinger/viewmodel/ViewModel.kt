package io.hellsinger.viewmodel

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel

abstract class ViewModel {
    abstract val viewModelScope: CoroutineScope

    open fun onDestroy() {
        viewModelScope.cancel()
    }
}
