package io.hellsinger.viewmodel

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

abstract class VortexViewModel<UiState, MutableUiState : UiState>(
    scopeName: String,
) : BitViewModel<UiState>(scopeName) {
    protected abstract val state: MutableUiState

    override fun getUiState(): UiState = state

    protected fun intent(
        context: CoroutineContext = EmptyCoroutineContext,
        start: CoroutineStart = CoroutineStart.DEFAULT,
        block: suspend CoroutineScope.(MutableUiState) -> Unit,
    ) = viewModelScope.launch(
        context = context,
        start = start,
        block = { block(state) },
    )

    protected fun update(
        context: CoroutineContext = EmptyCoroutineContext,
        start: CoroutineStart = CoroutineStart.DEFAULT,
        block: suspend CoroutineScope.(MutableUiState) -> Int,
    ) = viewModelScope.launch(
        context = context,
        start = start,
        block = { update(block(state)) },
    )
}
