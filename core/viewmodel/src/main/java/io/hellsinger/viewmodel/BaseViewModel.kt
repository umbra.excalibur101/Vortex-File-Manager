package io.hellsinger.viewmodel

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

abstract class BaseViewModel(
    scopeName: String,
) : ViewModel() {
    override val viewModelScope: CoroutineScope =
        CoroutineScope(Dispatchers.Main.immediate + CoroutineName(scopeName))
}
