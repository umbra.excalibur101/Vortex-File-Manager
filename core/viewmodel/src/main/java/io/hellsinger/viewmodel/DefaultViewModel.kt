package io.hellsinger.viewmodel

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

abstract class DefaultViewModel<UiState>(
    initial: UiState,
    scopeName: String,
) : BaseViewModel(scopeName) {
    protected val state: MutableStateFlow<UiState> = MutableStateFlow(initial)

    suspend fun update(state: UiState) {
        this.state.emit(state)
    }

    fun getState() = state.asStateFlow()
}
