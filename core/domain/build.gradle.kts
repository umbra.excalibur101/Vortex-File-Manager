plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.ktlint)
}

ktlint {
    outputColorName.set("RED")
}

dependencies {
    implementation(libs.kotlinx.coroutines.core)

    implementation(projects.filesystem)
}
