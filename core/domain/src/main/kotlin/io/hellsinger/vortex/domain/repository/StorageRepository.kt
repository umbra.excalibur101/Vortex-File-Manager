package io.hellsinger.vortex.domain.repository

import io.hellsinger.filesystem.attribute.PathGroup
import io.hellsinger.filesystem.attribute.PathUser
import io.hellsinger.filesystem.directory.DirectoryEntry
import io.hellsinger.filesystem.path.Path
import io.hellsinger.vortex.domain.model.Transition
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface StorageRepository {
    fun getPendingTransitions(): StateFlow<List<Transition>>

    suspend fun addPendingTransition(transition: Transition)

    suspend fun removePending(transition: Transition)

    suspend fun getEntries(path: Path): Flow<DirectoryEntry<Path>>

    suspend fun getUser(id: Int): PathUser

    suspend fun getGroup(id: Int): PathGroup

    suspend fun getBytes(
        path: Path,
        flags: Int,
    ): Flow<Byte>

    suspend fun check(
        path: Path,
        flags: Int,
    ): Int
}
