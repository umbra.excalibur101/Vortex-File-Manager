package io.hellsinger.vortex.domain.model

import io.hellsinger.filesystem.path.Path

sealed interface Transition {
    val sources: List<Path>

    data class Move(
        override val sources: List<Path>,
    ) : Transition

    data class Copy(
        override val sources: List<Path>,
    ) : Transition
}
