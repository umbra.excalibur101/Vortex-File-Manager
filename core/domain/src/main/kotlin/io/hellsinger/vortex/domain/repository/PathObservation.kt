package io.hellsinger.vortex.domain.repository

import io.hellsinger.filesystem.path.ObservablePath
import io.hellsinger.filesystem.path.Path
import kotlinx.coroutines.flow.Flow

interface PathObservation : AutoCloseable {
    val events: Flow<ObservablePath<Path>>

    fun addPath(
        path: Path,
        event: Int,
    )

    fun removePath(path: Path)

    fun removeAll()

    override fun close()
}
