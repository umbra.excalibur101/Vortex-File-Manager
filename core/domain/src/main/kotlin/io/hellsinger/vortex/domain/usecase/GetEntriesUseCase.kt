package io.hellsinger.vortex.domain.usecase

import io.hellsinger.filesystem.path.Path
import io.hellsinger.vortex.domain.repository.StorageRepository

// Added for future!
class GetEntriesUseCase(
    private val repository: StorageRepository,
) {
    suspend operator fun invoke(path: Path) = repository.getEntries(path)
}
