package io.hellsinger.vortex

import kotlinx.coroutines.Dispatchers

class DefaultDispatchers : io.hellsinger.vortex.Dispatchers {
    override val default = Dispatchers.Default
    override val io = Dispatchers.IO
    override val main = Dispatchers.Main
    override val immediate = Dispatchers.Main.immediate
}
