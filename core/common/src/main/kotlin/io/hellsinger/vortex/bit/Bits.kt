package io.hellsinger.vortex.bit

interface Bits {
    infix fun Int.removeFlag(flag: Int): Int = and(flag.inv())

    infix fun Int.addFlag(flag: Int): Int = or(flag)

    infix fun Int.hasFlag(flag: Int): Boolean = and(flag) == flag

    infix fun Int.toggleFlag(flag: Int): Int = if (hasFlag(flag)) removeFlag(flag) else addFlag(flag)

    fun Int.hasFlags(
        first: Int,
        second: Int,
    ) = hasFlag(first) || hasFlag(second)

    companion object : Bits
}
