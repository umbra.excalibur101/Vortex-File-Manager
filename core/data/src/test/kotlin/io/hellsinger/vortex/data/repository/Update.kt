@file:Suppress("ktlint")

package io.hellsinger.vortex.data.repository

import io.hellsinger.vortex.data.model.OperationState

suspend inline fun StorageOperationStateProducer.update(operationState: () -> OperationState.Transition) {
    update(operationState())
}
