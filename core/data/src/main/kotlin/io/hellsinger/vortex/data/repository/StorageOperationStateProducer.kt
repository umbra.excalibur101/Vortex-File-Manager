package io.hellsinger.vortex.data.repository

import io.hellsinger.vortex.data.model.OperationState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface StorageOperationStateProducer : StorageOperationStateConsumer {
    override fun getState(): Flow<OperationState>

    suspend fun update(operationState: OperationState)
}
