package io.hellsinger.vortex.data.repository

import io.hellsinger.filesystem.path.Path
import io.hellsinger.vortex.data.repository.AndroidStorageRepository.Companion.DATA_DIRECTORY_PATH
import io.hellsinger.vortex.data.repository.AndroidStorageRepository.Companion.OBB_DIRECTORY_PATH

class AndroidRestrictedStorageRepository {
    fun isRestricted(path: Path): Boolean = path == DATA_DIRECTORY_PATH || path == OBB_DIRECTORY_PATH
}
