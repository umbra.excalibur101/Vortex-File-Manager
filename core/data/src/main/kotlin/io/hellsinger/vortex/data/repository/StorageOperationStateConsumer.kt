package io.hellsinger.vortex.data.repository

import io.hellsinger.vortex.data.model.OperationState
import kotlinx.coroutines.flow.Flow

interface StorageOperationStateConsumer {
    fun getState(): Flow<OperationState>
}
