package io.hellsinger.vortex.data.model

import io.hellsinger.filesystem.attribute.mimetype.MIME_TYPE_APPLICATION
import io.hellsinger.filesystem.attribute.mimetype.MIME_TYPE_AUDIO
import io.hellsinger.filesystem.attribute.mimetype.MIME_TYPE_IMAGE
import io.hellsinger.filesystem.attribute.mimetype.MIME_TYPE_TEXT
import io.hellsinger.filesystem.attribute.mimetype.MIME_TYPE_VIDEO
import io.hellsinger.filesystem.attribute.mimetype.MimeType
import io.hellsinger.theme.vortex.VortexSettings

fun MimeType.parseThemeType(): String? =
    when (type) {
        MIME_TYPE_APPLICATION -> VortexSettings.texts { key_storage_path_item_mime_type_application }
        MIME_TYPE_VIDEO -> VortexSettings.texts { key_storage_path_item_mime_type_video }
        MIME_TYPE_IMAGE -> VortexSettings.texts { key_storage_path_item_mime_type_image }
        MIME_TYPE_TEXT -> VortexSettings.texts { key_storage_path_item_mime_type_text }
        MIME_TYPE_AUDIO -> VortexSettings.texts { key_storage_path_item_mime_type_audio }
        else -> null
    }
