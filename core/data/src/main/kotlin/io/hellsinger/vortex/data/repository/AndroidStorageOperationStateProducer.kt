package io.hellsinger.vortex.data.repository

import io.hellsinger.vortex.data.model.OperationState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class AndroidStorageOperationStateProducer : StorageOperationStateProducer {
    private val state = MutableStateFlow<OperationState>(OperationState.Pending)

    override suspend fun update(operationState: OperationState) {
        state.emit(operationState)
    }

    override fun getState(): StateFlow<OperationState> = state
}
