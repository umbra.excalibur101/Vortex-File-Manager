package io.hellsinger.vortex.data.repository

import io.hellsinger.filesystem.linux.operation.LinuxPathObserver
import io.hellsinger.filesystem.path.ObservablePath
import io.hellsinger.filesystem.path.Path
import io.hellsinger.vortex.domain.repository.PathObservation
import kotlinx.coroutines.flow.Flow

class AndroidPathObservation(
    private val observer: LinuxPathObserver,
) : PathObservation {
    override val events: Flow<ObservablePath<Path>>
        get() = observer.events

    override fun addPath(
        path: Path,
        event: Int,
    ) {
        observer.add(path, event)
    }

    override fun removePath(path: Path) {
        observer.remove(path)
    }

    override fun removeAll() {
        observer.clear()
    }

    override fun close() {
        observer.close()
    }
}
