package io.hellsinger.vortex.data.model

sealed interface OperationState {
    val id: Int
    val type: Int

    data object Pending : OperationState {
        override val type: Int
            get() = -1
        override val id: Int
            get() = -1
    }

    data class Modification(
        override val id: Int,
        override val type: Int,
        val progress: Long,
        val max: Long,
        val source: PathItem,
    ) : OperationState

    data class Transition(
        override val id: Int,
        override val type: Int,
        val progress: Long,
        val max: Long,
        val source: PathItem,
        val dest: PathItem,
    ) : OperationState
}
