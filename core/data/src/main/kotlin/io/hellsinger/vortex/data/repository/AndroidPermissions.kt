package io.hellsinger.vortex.data.repository

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Context
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Build
import android.os.Environment
import android.os.Process
import androidx.annotation.RequiresApi

class AndroidPermissions(
    private val context: Context,
) {
    @RequiresApi(Build.VERSION_CODES.R)
    fun requiresFullStorageAccess(): Boolean = !Environment.isExternalStorageManager()

    fun requiresReadPermission() = check(READ_EXTERNAL_STORAGE)

    fun requireWritePermission() = check(WRITE_EXTERNAL_STORAGE)

    fun check(permission: String): Boolean =
        context.checkPermission(
            permission,
            Process.myPid(),
            Process.myUid(),
        ) != PERMISSION_GRANTED
}
