plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.ktlint)
}

ktlint {
    android.set(true)
    outputColorName.set("RED")
}

android {
    namespace = "io.hellsinger.vortex.navigation"
    compileSdk = AndroidConfigure.targetSdk

    defaultConfig {
        minSdk = AndroidConfigure.minSdk

        consumerProguardFiles("consumer-rules.pro")
    }

    compileOptions {
        sourceCompatibility = BuildConfig.JDK.VerEnum
        targetCompatibility = BuildConfig.JDK.VerEnum
    }
}

dependencies {
    implementation(projects.core.ui)

    implementation(libs.kotlinx.coroutines.android)
}
