package io.hellsinger.vortex.navigation

import android.content.Context
import android.widget.FrameLayout
import io.hellsinger.vortex.ui.component.navigation.NavigationBarView
import kotlin.reflect.KProperty

interface GlobalUiProvider {
    val root: FrameLayout?
    val bar: NavigationBarView?
}

fun globalUiProvider(context: Context): GlobalUiProvider? = if (context is GlobalUiProvider) context else null

operator fun Context.getValue(
    thisRef: Any?,
    property: KProperty<*>,
): NavigationBarView? = globalUiProvider(this)?.bar
