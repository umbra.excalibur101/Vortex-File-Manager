package io.hellsinger.vortex.navigation

import android.content.Context

interface ComponentProvider<T> {
    fun provide(): T
}

inline fun <reified T> Context.findDependencies(): T =
    checkNotNull(applicationContext as? ComponentProvider<T>) {
        "Application doesn't implement dependency provider"
    }.provide()
