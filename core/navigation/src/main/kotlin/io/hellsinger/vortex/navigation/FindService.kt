package io.hellsinger.vortex.navigation

import android.content.Context
import kotlinx.coroutines.flow.Flow

interface ServiceProvider<Service> {
    fun getService(): Flow<Service>
}

fun <Service> Context.findService(): Flow<Service> {
    if (this is ServiceProvider<*>) {
        return getService() as Flow<Service>
    }
    throw IllegalArgumentException("Context does not implement ServiceProvider class")
}
