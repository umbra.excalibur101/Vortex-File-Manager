package io.hellsinger.vortex.ui.component.adapter

interface Item<T> {
    val id: Long

    val viewType: Int

    val value: T
}

typealias SuperItem = Item<*>
