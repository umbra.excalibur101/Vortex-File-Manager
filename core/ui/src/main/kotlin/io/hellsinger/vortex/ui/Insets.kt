package io.hellsinger.vortex.ui

import android.view.WindowInsets
import androidx.core.view.WindowInsetsCompat

inline fun withCompatInsets(
    insets: WindowInsets,
    block: WindowInsetsCompat.() -> WindowInsets,
): WindowInsets = WindowInsetsCompat.toWindowInsetsCompat(insets).block()
