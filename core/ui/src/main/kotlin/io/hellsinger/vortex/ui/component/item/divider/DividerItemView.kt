package io.hellsinger.vortex.ui.component.item.divider

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.view.View
import android.view.ViewGroup.LayoutParams
import androidx.recyclerview.widget.RecyclerView
import io.hellsinger.vortex.ui.component.dp

class DividerItemView(
    context: Context,
) : View(context) {
    private val paint =
        Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG).apply {
            style = Paint.Style.FILL
        }

    init {
        layoutParams =
            RecyclerView.LayoutParams(LayoutParams.MATCH_PARENT, 1.dp).apply {
                topMargin = 8.dp
            }
    }

    override fun onDraw(canvas: Canvas) {
        canvas.drawRect(
            paddingStart.toFloat(),
            paddingTop.toFloat(),
            (width - paddingEnd).toFloat(),
            (paddingStart + height + paddingBottom).toFloat(),
            paint,
        )
    }

    fun setColor(color: Int) {
        paint.color = color
        invalidate()
    }
}
