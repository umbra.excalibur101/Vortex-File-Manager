package io.hellsinger.vortex.ui.component

import android.view.View
import android.view.ViewGroup
import androidx.annotation.Px

operator fun ViewGroup.iterator(): MutableIterator<View> =
    object : MutableIterator<View> {
        private var index = 0

        override fun hasNext() = index < childCount

        override fun next() = getChildAt(index++) ?: throw IndexOutOfBoundsException()

        override fun remove() = removeViewAt(--index)
    }

val ViewGroup.children: Sequence<View>
    get() =
        object : Sequence<View> {
            override fun iterator() = this@children.iterator()
        }

inline operator fun ViewGroup.contains(view: View): Boolean = indexOfChild(view) != -1

inline fun View.updatePadding(
    @Px left: Int = paddingLeft,
    @Px top: Int = paddingTop,
    @Px right: Int = paddingRight,
    @Px bottom: Int = paddingBottom,
) {
    setPadding(left, top, right, bottom)
}

inline fun View.updateLayoutParams(block: ViewGroup.LayoutParams.() -> Unit) {
    updateLayoutParams<ViewGroup.LayoutParams>(block)
}

@JvmName("updateLayoutParamsTyped")
inline fun <reified T : ViewGroup.LayoutParams> View.updateLayoutParams(block: T.() -> Unit) {
    val params = layoutParams as T
    block(params)
    layoutParams = params
}
