package io.hellsinger.vortex.ui.component.item.twoline

import android.graphics.drawable.Drawable

data class TwoLineItem(
    val title: String,
    val description: String,
    val icon: Drawable? = null,
)
