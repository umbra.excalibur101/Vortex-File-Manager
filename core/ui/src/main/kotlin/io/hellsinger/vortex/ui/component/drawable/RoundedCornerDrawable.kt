package io.hellsinger.vortex.ui.component.drawable

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import io.hellsinger.vortex.ui.component.Paints

class RoundedCornerDrawable(
    private var color: Int,
    private var topLeft: Float = 0F,
    private var topRight: Float = 0F,
    private var bottomLeft: Float = 0F,
    private var bottomRight: Float = 0F,
    private val paddingLeft: Int = 0,
    private val paddingTop: Int = 0,
    private val paddingRight: Int = 0,
    private val paddingBottom: Int = 0,
) : Drawable() {
    private var paint: Paint? = null
    private val path = Path()

    constructor(
        color: Long,
        topLeft: Float = 0F,
        topRight: Float = 0F,
        bottomLeft: Float = 0F,
        bottomRight: Float = 0F,
        paddingLeft: Int = 0,
        paddingTop: Int = 0,
        paddingRight: Int = 0,
        paddingBottom: Int = 0,
    ) : this(
        color.toInt(),
        topLeft,
        topRight,
        bottomLeft,
        bottomRight,
        paddingLeft,
        paddingTop,
        paddingRight,
        paddingBottom,
    )

    constructor(
        color: Int,
        radius: Float = 0F,
        paddingLeft: Int = 0,
        paddingTop: Int = 0,
        paddingRight: Int = 0,
        paddingBottom: Int = 0,
    ) : this(
        color,
        radius,
        radius,
        radius,
        radius,
        paddingLeft,
        paddingTop,
        paddingRight,
        paddingBottom,
    )

    constructor(
        color: Long,
        radius: Float = 0F,
        paddingLeft: Int = 0,
        paddingTop: Int = 0,
        paddingRight: Int = 0,
        paddingBottom: Int = 0,
    ) : this(color.toInt(), radius, paddingLeft, paddingTop, paddingRight, paddingBottom)

    constructor(
        color: Long,
        radius: Float = 0F,
        padding: Int = 0,
    ) : this(color.toInt(), radius, padding, padding, padding, padding)

    constructor(
        paint: Paint,
        radius: Float = 0F,
        padding: Int = 0,
    ) : this(0, radius, radius, radius, radius, padding, padding, padding, padding) {
        this.paint = paint
    }

    override fun draw(canvas: Canvas) {
        val rounded = Paints.rounded()
        rounded.set(
            (bounds.left + paddingLeft).toFloat(),
            (bounds.top + paddingTop).toFloat(),
            (bounds.right - paddingRight).toFloat(),
            (bounds.bottom - paddingBottom).toFloat(),
        )

        path.addRoundRect(
            rounded,
            floatArrayOf(
                topLeft,
                topLeft,
                topRight,
                topRight,
                bottomLeft,
                bottomLeft,
                bottomRight,
                bottomRight,
            ),
            Path.Direction.CW,
        )

        canvas.drawPath(
            path,
            paint ?: Paints.filling(color),
        )
    }

    fun setElevation(elevation: Float) {
        paint?.setShadowLayer(elevation, 0F, 0F, 0x18000000)
    }

    fun setColor(color: Int) {
        this.color = color
        paint?.color = color
        invalidateSelf()
    }

    fun getColor() = color

    override fun setAlpha(alpha: Int) {
        paint?.alpha = alpha
    }

    override fun setColorFilter(filter: ColorFilter?) {
        paint?.colorFilter = colorFilter
    }

    @Deprecated(
        "Deprecated in Java",
        ReplaceWith("PixelFormat.UNKNOWN", "android.graphics.PixelFormat"),
    )
    override fun getOpacity(): Int = PixelFormat.UNKNOWN
}
