package io.hellsinger.vortex.ui.component.adapter

import android.graphics.drawable.Drawable
import android.view.View
import android.view.View.OnClickListener
import android.view.View.OnLongClickListener
import android.view.ViewGroup
import io.hellsinger.vortex.ui.component.adapter.holder.RecyclableView
import io.hellsinger.vortex.ui.component.item.button.ButtonItemView
import io.hellsinger.vortex.ui.component.item.divider.DividerItemView
import io.hellsinger.vortex.ui.component.item.drawer.DrawerItemView
import io.hellsinger.vortex.ui.component.item.title.TitleItemView
import io.hellsinger.vortex.ui.component.item.twoline.TwoLineItemView

object Items {
    const val NO_TYPE = -1
    const val TITLE = 0
    const val DIVIDER = 1
    const val DRAWER = 2
    const val SWITCH = 3
    const val TWO_LINE = 4
    const val BUTTON = 5

    fun resolveViewType(
        parent: ViewGroup,
        viewType: Int,
    ): View =
        when (viewType) {
            TITLE -> TitleItemView(parent.context)
            DIVIDER -> DividerItemView(parent.context)
            DRAWER -> DrawerItemView(parent.context)
            TWO_LINE -> TwoLineItemView(parent.context)
            BUTTON -> ButtonItemView(parent.context)
            else -> throw UnsupportedViewTypeException(viewType)
        }

    fun resolveViewTypeListener(
        view: View,
        clickListener: OnClickListener?,
        longClickListener: OnLongClickListener?,
    ) {
        if (view is RecyclableView<*>) {
            view.onBindListener(clickListener)
            view.onBindLongListener(longClickListener)
        }
    }
}

fun MutableList<Item<*>>.title(
    content: String,
    index: Int = size,
) = add(index, ItemFactory.Title(content))

fun MutableList<Item<*>>.drawer(
    id: Int,
    title: String,
    icon: Drawable?,
    index: Int = size,
) = add(index, ItemFactory.Drawer(id, title, icon))

fun MutableList<Item<*>>.divider() = add(ItemFactory.Divider)

fun MutableList<Item<*>>.twoLine(
    title: String,
    desc: String,
    index: Int = size,
) = add(index, ItemFactory.TwoLine(title, desc))
