package io.hellsinger.vortex.ui.component.item.button

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Margins(
    val top: Int,
    val bottom: Int,
    val left: Int,
    val right: Int,
) : Parcelable

data class ButtonItem(
    val id: Int,
    val text: String,
    val margins: Margins = Margins(8, 8, 8, 8),
)
