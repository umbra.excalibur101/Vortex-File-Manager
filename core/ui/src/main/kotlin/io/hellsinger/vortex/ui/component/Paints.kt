package io.hellsinger.vortex.ui.component

import android.graphics.Paint
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.graphics.Paint.DITHER_FLAG
import android.graphics.RectF

object Paints {
    private var rectF: RectF? = null
    private var filling: Paint? = null

    fun rounded(): RectF {
        if (rectF == null) rectF = RectF()
        return rectF!!
    }

    fun filling(color: Long) = filling(color.toInt())

    fun filling(color: Int): Paint {
        if (filling == null) {
            filling =
                Paint(ANTI_ALIAS_FLAG or DITHER_FLAG).apply {
                    style = Paint.Style.FILL
                }
        }
        filling!!.color = color
        return filling!!
    }
}
