package io.hellsinger.vortex.ui.component.item.button

import android.content.Context
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.Button
import android.widget.FrameLayout
import io.hellsinger.vortex.ui.component.adapter.holder.RecyclableView
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.updateLayoutParams

class ButtonItemView(
    context: Context,
) : Button(context),
    RecyclableView<ButtonItem> {
    init {
        layoutParams = FrameLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
    }

    override fun onBind(item: ButtonItem) {
        id = item.id
        text = item.text
        updateLayoutParams<FrameLayout.LayoutParams> {
            leftMargin = item.margins.left.dp
            rightMargin = item.margins.right.dp
            topMargin = item.margins.top.dp
            bottomMargin = item.margins.bottom.dp
        }
    }

    override fun onBindListener(listener: OnClickListener?) {
        setOnClickListener(listener)
    }

    override fun onBindLongListener(listener: OnLongClickListener?) {
        setOnLongClickListener(listener)
    }
}
