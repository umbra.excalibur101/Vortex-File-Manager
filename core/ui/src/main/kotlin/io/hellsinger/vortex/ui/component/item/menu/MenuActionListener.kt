package io.hellsinger.vortex.ui.component.item.menu

fun interface MenuActionListener {
    fun onMenuActionCall(id: Int): Boolean
}
