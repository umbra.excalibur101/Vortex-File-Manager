package io.hellsinger.vortex.ui.component.adapter

class UnsupportedViewTypeException(
    viewType: Int,
) : Throwable("Unsupported view type (viewType: $viewType)")
