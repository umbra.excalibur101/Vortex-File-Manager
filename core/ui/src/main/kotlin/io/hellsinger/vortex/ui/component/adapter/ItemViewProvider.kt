package io.hellsinger.vortex.ui.component.adapter

import android.view.View
import android.view.ViewGroup

fun interface ItemViewProvider<V : View> {
    operator fun invoke(
        parent: ViewGroup,
        viewType: Int = Items.NO_TYPE,
    ) = provide(parent, viewType)

    fun provide(parent: ViewGroup) = provide(parent, Items.NO_TYPE)

    fun provide(
        parent: ViewGroup,
        viewType: Int,
    ): V
}
