package io.hellsinger.vortex.ui.component.adapter.listener

import android.view.View

interface ItemListener {
    fun onItemClick(
        view: View,
        position: Int,
    )

    fun onLongItemClick(
        view: View,
        position: Int,
    ): Boolean
}
