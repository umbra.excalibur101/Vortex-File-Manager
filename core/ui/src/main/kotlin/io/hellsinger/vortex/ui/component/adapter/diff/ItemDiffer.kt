package io.hellsinger.vortex.ui.component.adapter.diff

import androidx.recyclerview.widget.DiffUtil
import io.hellsinger.vortex.ui.component.adapter.Item

class ItemDiffer(
    private val old: List<Item<*>>,
    private val new: List<Item<*>>,
) : DiffUtil.Callback() {
    override fun getOldListSize() = old.size

    override fun getNewListSize() = new.size

    override fun areItemsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int,
    ): Boolean = old[oldItemPosition].id == new[newItemPosition].id

    override fun areContentsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int,
    ): Boolean = old[oldItemPosition] == new[newItemPosition]
}
