package io.hellsinger.vortex.ui.component.adapter

import androidx.recyclerview.widget.RecyclerView.ViewHolder

fun interface ItemViewBinder<T, VH : ViewHolder> {
    operator fun invoke(
        holder: VH,
        item: T,
    ) = bind(holder, item)

    fun bind(
        holder: VH,
        item: T,
    )
}
