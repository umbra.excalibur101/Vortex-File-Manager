package io.hellsinger.vortex.ui.component.adapter

fun interface ListRenderer {
    fun render(): List<Item<*>>
}
