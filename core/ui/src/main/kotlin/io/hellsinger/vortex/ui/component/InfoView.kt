package io.hellsinger.vortex.ui.component

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.view.Gravity
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

class InfoView(
    context: Context,
) : LinearLayout(context) {
    private val iconView =
        ImageView(context).apply {
            minimumHeight = 56.dp
            minimumWidth = 56.dp
        }

    private val messageView =
        TextView(context).apply {
            textSize = 20F
            textAlignment = TEXT_ALIGNMENT_CENTER
        }

    var icon: Drawable?
        get() = iconView.drawable
        set(value) {
            iconView.setImageDrawable(value)
        }

    var message: CharSequence?
        get() = messageView.text
        set(value) {
            if (value != messageView.text) messageView.text = value
        }

    fun setMessageTextColor(color: Int) {
        messageView.setTextColor(color)
    }

    fun setIconColor(color: Int) {
        iconView.imageTintList = ColorStateList.valueOf(color)
    }

    init {
        orientation = VERTICAL
        gravity = Gravity.CENTER
        addView(iconView, LayoutParams(WRAP_CONTENT, WRAP_CONTENT))
        addView(messageView, LayoutParams(MATCH_PARENT, WRAP_CONTENT))
    }
}
