package io.hellsinger.vortex.ui.component.adapter

import android.graphics.drawable.Drawable
import android.view.View
import io.hellsinger.vortex.ui.component.item.menu.MenuAction
import io.hellsinger.vortex.ui.component.item.twoline.TwoLineItem

@Suppress("ktlint:standard:function-naming")
object ItemFactory

val ItemFactory.Divider: Item<Unit>
    get() =
        object : Item<Unit> {
            override val id: Long
                get() = Long.MIN_VALUE

            override val value: Unit
                get() = Unit

            override val viewType: Int
                get() = Items.DIVIDER
        }

fun ItemFactory.Title(
    value: String,
    id: Int = View.NO_ID,
) = object : Item<String> {
    override val id: Long
        get() = if (id != View.NO_ID) id.toLong() else value.hashCode().toLong()
    override val viewType: Int
        get() = Items.TITLE
    override val value: String
        get() = value
}

fun ItemFactory.Drawer(
    id: Int,
    title: String,
    icon: Drawable? = null,
) = object : Item<MenuAction> {
    override val id: Long
        get() = id.toLong()
    override val viewType: Int
        get() = Items.DRAWER
    override val value: MenuAction = MenuAction(id, title, icon)
}

fun ItemFactory.TwoLine(
    title: String,
    description: String,
    id: Int = View.NO_ID,
) = object : Item<TwoLineItem> {
    override val id: Long
        get() = if (id != View.NO_ID) id.toLong() else value.hashCode().toLong()
    override val viewType: Int
        get() = Items.TWO_LINE
    override val value: TwoLineItem = TwoLineItem(title, description)
}
