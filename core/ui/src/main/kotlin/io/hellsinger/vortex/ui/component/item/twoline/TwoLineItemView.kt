package io.hellsinger.vortex.ui.component.item.twoline

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.RippleDrawable
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import io.hellsinger.vortex.ui.component.adapter.holder.RecyclableView
import io.hellsinger.vortex.ui.component.contains
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.drawable.RoundedCornerDrawable
import io.hellsinger.vortex.ui.component.updatePadding
import io.hellsinger.vortex.ui.dsl.params
import io.hellsinger.vortex.ui.dsl.text

class TwoLineItemView(
    context: Context,
) : LinearLayout(context),
    RecyclableView<TwoLineItem> {
    private val surface = RoundedCornerDrawable(color = 0)

    private val background =
        RippleDrawable(
            ColorStateList.valueOf(
                0,
            ),
            surface,
            null,
        )

    private val iconView =
        ImageView(context).apply {
        }

    private val titleView =
        TextView(context).apply {
            textSize = 16F
        }

    private val descriptionView =
        TextView(context).apply {
            textSize = 14F
        }

    private fun ensureContainingTitle() {
        if (!contains(titleView)) {
            addView(titleView)
        }
    }

    private fun ensureContainingDescription() {
        if (!contains(descriptionView)) {
            addView(descriptionView)
        }
    }

    var title: String?
        get() = titleView.text.toString()
        set(value) {
            ensureContainingTitle()
            titleView.text = value
        }

    var description: String?
        get() = descriptionView.text.toString()
        set(value) {
            ensureContainingDescription()
            descriptionView.text = value
        }

    init {
        orientation = VERTICAL
        params(MATCH_PARENT, WRAP_CONTENT)
        updatePadding(
            left = 16.dp,
            right = 16.dp,
            top = 8.dp,
            bottom = 8.dp,
        )
        isClickable = true
        isFocusable = true
        setBackground(background)
    }

    override fun setBackgroundColor(color: Int) = surface.setColor(color)

    fun setRippleColor(color: Int) = background.setColor(ColorStateList.valueOf(color))

    fun setTitleColor(color: Int) = titleView.setTextColor(color)

    fun setDescriptionColor(color: Int) = descriptionView.setTextColor(color)

    override fun onBind(item: TwoLineItem) {
        title = item.title
        description = item.description
        iconView.setImageDrawable(item.icon)
    }

    override fun onUnbind() {
        title = null
        description = null
    }

    override fun onBindListener(listener: OnClickListener?) {
        setOnClickListener(listener)
    }

    override fun onBindLongListener(listener: OnLongClickListener?) {
    }

    override fun onUnbindListeners() {
        setOnClickListener(null)
    }
}
