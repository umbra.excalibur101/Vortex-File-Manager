package io.hellsinger.vortex.ui.component.item.menu

import android.content.Context
import android.view.View.MeasureSpec.AT_MOST
import android.view.View.MeasureSpec.EXACTLY
import android.view.View.MeasureSpec.getMode
import android.view.View.MeasureSpec.getSize
import android.view.View.MeasureSpec.makeMeasureSpec
import android.widget.FrameLayout
import io.hellsinger.theme.Colors
import io.hellsinger.vortex.ui.component.dp
import java.lang.Integer.max
import kotlin.math.min

class MenuLayout(
    context: Context,
) : FrameLayout(context) {
    private val items = mutableListOf<MenuAction>()
    private val views = mutableListOf<MenuItemView>()

    private val listeners = mutableListOf<MenuActionListener>()

    private var color: Int = Colors.White.toInt()
    private var rippleColor: Int = Colors.White.toInt()

    fun setItemColor(color: Int) {
        this.color = color
    }

    fun setItemRippleColor(color: Int) {
        this.rippleColor = color
    }

    val isVertical: Boolean
        get() = false

    fun addItem(action: MenuAction) {
        items += action
        initViews()
    }

    fun removeItem(action: MenuAction) {
        items -= action
        initViews()
    }

    fun replaceItems(actions: Collection<MenuAction>) {
        items.clear()
        items.addAll(actions)
        initViews()
    }

    init {
        initViews()
    }

    fun addListener(listener: MenuActionListener) {
        listeners.add(listener)
    }

    fun removeListener(listener: MenuActionListener) {
        listeners.remove(listener)
    }

    private fun initViews() {
        removeAllViews()
        views.clear()
        for (item in items) views.add(getItem(item))
        for (view in views) addView(view)
    }

    private fun getItem(item: MenuAction): MenuItemView =
        MenuItemView(context).apply {
            action = item
            titleColor = color
            iconColor = color
            setRippleColor(rippleColor)
            setOnClickListener { notify(item) }
        }

    private fun notify(item: MenuAction) {
        for (listener in listeners) {
            if (listener.onMenuActionCall(item.id)) break
        }
    }

    override fun onMeasure(
        widthMeasureSpec: Int,
        heightMeasureSpec: Int,
    ) {
        val widthSize = getSize(widthMeasureSpec) // requires full width-size
        val widthMode = getMode(widthMeasureSpec)
        val heightSize = getSize(heightMeasureSpec)
        val heightMode = getMode(heightMeasureSpec)

        val width =
            when (widthMode) {
                EXACTLY -> widthSize
                AT_MOST -> max(56.dp, widthSize)
                else -> 56.dp
            }

        val height =
            when (heightMode) {
                EXACTLY -> heightSize
                AT_MOST -> min(56.dp, heightSize)
                else -> 56.dp
            }

        views.forEach { item ->
            item.measure(
                makeMeasureSpec(24.dp, EXACTLY),
                makeMeasureSpec(24.dp, EXACTLY),
            )
        }
        setMeasuredDimension(width, height)
    }

    override fun onLayout(
        changed: Boolean,
        l: Int,
        t: Int,
        r: Int,
        b: Int,
    ) {
        var leftWidth = measuredWidth - 16.dp
        var topHeight = (if (isVertical) 0 else height / 2)

        views.forEachIndexed { index, item ->
            item.layout(
                leftWidth - item.measuredWidth,
                height / 2 - item.measuredHeight,
                leftWidth,
                height / 2 + item.measuredHeight,
            )
            leftWidth = leftWidth - item.measuredWidth - 24.dp
        }
    }
}
