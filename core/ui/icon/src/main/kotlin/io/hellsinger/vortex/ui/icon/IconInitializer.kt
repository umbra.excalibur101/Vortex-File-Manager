package io.hellsinger.vortex.ui.icon

import android.content.Context

// TODO: Rewrite with interface and provide with Dagger
object IconInitializer : IIconInitializer {
    var context: Context? = null
        get() = field ?: throw IllegalArgumentException()
        set(value) {
            field = value?.applicationContext
        }

    override operator fun get(id: Int) = context!!.getDrawable(id)
}
