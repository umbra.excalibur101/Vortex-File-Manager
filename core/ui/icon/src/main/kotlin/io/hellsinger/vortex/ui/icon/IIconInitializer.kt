package io.hellsinger.vortex.ui.icon

import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes

interface IIconInitializer {
    operator fun get(
        @DrawableRes id: Int,
    ): Drawable?
}
