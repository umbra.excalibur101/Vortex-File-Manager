package io.hellsinger.vortex.foundation.ui.toast

import android.content.Context
import android.widget.Toast

fun <T : Any> T.toastIt(
    context: Context,
    message: String = "",
    duration: Int = Toast.LENGTH_SHORT,
): T {
    Toast.makeText(context, message + this.toString(), duration).show()
    return this
}
