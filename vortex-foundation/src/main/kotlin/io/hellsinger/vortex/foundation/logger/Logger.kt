package io.hellsinger.vortex.foundation.logger

import android.util.Log

fun <T> T.logIt(
    tag: String = "Loggable",
    suffix: String = "",
    type: Int = Log.INFO,
): T {
    Log.println(type, tag, this.toString() + suffix)
    return this
}
