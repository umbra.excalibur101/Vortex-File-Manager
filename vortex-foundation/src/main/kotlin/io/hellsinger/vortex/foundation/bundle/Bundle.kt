package io.hellsinger.vortex.foundation.bundle

import android.os.Bundle
import android.os.Parcelable
import io.hellsinger.vortex.foundation.isAtLeastAndroid13
import java.io.Serializable

inline fun <reified T : Parcelable> Bundle.parcelable(key: String): T? =
    when {
        isAtLeastAndroid13 -> getParcelable(key, T::class.java)
        else ->
            @Suppress("DEPRECATION")
            getParcelable(key)
                as? T
    }

inline fun <reified T : Parcelable> Bundle.parcelable(
    key: String,
    default: T,
): T = parcelable<T>(key) ?: default

inline fun <reified T : Serializable> Bundle.serializable(key: String): T? =
    when {
        isAtLeastAndroid13 -> getSerializable(key, T::class.java)
        else ->
            @Suppress("DEPRECATION")
            getSerializable(key)
                as? T
    }

inline fun <reified T : Serializable> Bundle.serializable(
    key: String,
    default: T,
): T = serializable<T>(key) ?: default
